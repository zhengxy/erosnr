from matplotlib import pyplot as plt
from astropy.io import fits
import astropy.wcs
import numpy as np
import pyregion
import argparse
import sys
import math
import os
import select_regions
import shutil


def GetRegionBoundingBox(reg):

    x1, x2, y1, y2 = 0, 0, 0, 0

    if reg.name == "polygon":

        x_coords = []
        y_coords = []

        #print(reg.coord_list)
        for i in range(0, len(reg.coord_list), 2):
            x_coords.append(reg.coord_list[i])
            y_coords.append(reg.coord_list[i+1])

        x1 = int(min(x_coords)*0.9)
        x2 = int(max(x_coords)*1.1)
        y1 = int(min(y_coords)*0.9) 
        y2 = int(max(y_coords)*1.1)


    if reg.name == "circle":
        x = reg.coord_list[0]
        y = reg.coord_list[1]

        r = reg.coord_list[2]

        x1 = (x-r)*0.9
        x2 = (x+r)*1.1
        y1 = (y-r)*0.9
        y2 = (y+r)*1.1

    return int(x1), int(x2), int(y1), int(y2)

def CreateRegionMask(image, regfile, regfile_exclude, outfile, clobber):

    hdu = fits.open(image)[0]
    img = hdu.data

    wcs = astropy.wcs.WCS(hdu.header)

    img_mask = np.zeros(img.shape)

    regions = pyregion.open(regfile).as_imagecoord(hdu.header)
    source_reg = select_regions.ParseRegFile(regfile)
  

    if regfile_exclude != None:
        regions_exclude = pyregion.open(regfile_exclude).as_imagecoord(hdu.header)
        print("Using excluding regfile %s." % regfile_exclude)


    # loop over input regions
    for i in range(len(regions)):

        area = 0

        reg = regions[i:i+1]
        filt = reg.get_filter()

        # get bounding box of polygon for quicker program run
        x1, x2, y1, y2 = GetRegionBoundingBox(regions[i])

        excluded_pixels = 0
        excluded_pixels_ps = 0
        px_include = 0

        if regfile_exclude != None:
            filt_ex = regions_exclude.get_filter() 
        

        # check which pixels are in mask
        for x in range(max(x1, 0), min(x2,img.shape[0])):
            for y in range(max(y1, 0), min(y2, img.shape[1])):  

                
                if filt.inside1(x, y):
                    in_exclude = False

                    if regfile_exclude != None:
                        if filt_ex.inside1(x, y):
                            in_exclude = True
                            excluded_pixels += 1              
                            
                    if not in_exclude:
                        # if not excluded, set mask value for this pixel to 1 (equal to included in srctool)
                        img_mask[y, x] = 1
                        area += astropy.wcs.utils.proj_plane_pixel_area(wcs)
                        px_include += 1


        if regfile_exclude != None:
            print("Excluded pixels based on excluding file:",excluded_pixels, "(%f%%)" % (excluded_pixels/px_include*100))

        try:
            print("Area of polygon: ", area, "Unit:", wcs.wcs.cunit[0], "x", wcs.wcs.cunit[1])
        except: pass

    # write output file
    hdu_out = fits.PrimaryHDU(img_mask, header=hdu.header)
    hdul = fits.HDUList([hdu_out])

    try:
        hdul.writeto(outfile)
    except OSError:
        if clobber:
            hdul.writeto(outfile, overwrite=True)
            print("Mask created successfully.")
        else:
            print("Output file already existing, consider the --clobber option.")
    else:
        print("Mask created successfully.")

    
    if os.path.exists("mask_image.fits"):

        if clobber:
            try:
                shutil.copyfile(outfile, "mask_image.fits")
                print("Wrote mask for srctool to mask_image.fits. Usage in srctool: srcreg='mask mask_image.fits' srccoord='fk5;%f,%f'" % (source_reg[0]["ra"], source_reg[0]["dec"] ))
            except:
                print("Could not write mask_image.fits")
        else:
            print("mask_image.fits already exists, consider the --clobber option to overwrite and run script again.")
    else:
        shutil.copyfile(outfile, "mask_image.fits")
        print("Wrote mask for srctool to mask_image.fits. Usage in srctool: srcreg='mask mask_image.fits' srccoord='fk5;%f,%f'" % (source_reg[0]["ra"], source_reg[0]["dec"] ))

    unit =  wcs.wcs.cunit[0].to_string() + " x " + wcs.wcs.cunit[1].to_string()

    return area, unit

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--image", type=str, required=True, help="Input image (should be from same events used in the spectral analysis).")
    parser.add_argument("--regfile",type=str, required=True, help="ds9 region (fk5) file with a polygon OR circle region.")
    parser.add_argument("--regfile_exclude",type=str, required=False, help="(Optional) ds9 region file (fk5) for excluding certain regions from the source-region given in --regfile. Can be used together with --exclude_ps.")
    parser.add_argument("--outfile", type=str, required=True, help="Filename for the output mask map for srctool. (should end with .fits). Additionally mask_image.fits will be written (necessary for srctool).")
    parser.add_argument("--clobber", action="store_true", required=False, help="Overwrite outfile.")

    args = parser.parse_args()

    CreateRegionMask(args.image, args.regfile, args.regfile_exclude, args.outfile, args.clobber)
