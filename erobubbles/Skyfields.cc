

using namespace std;
#include<iostream>
#include<cstdio>   
#include<cstring>          
#include<valarray>
#include"fitsio.h"
#include <sstream>
#include <string>

#include"Skyfields.h"

////////////////////////////////////////////////////////

void Skyfields:: read_SKYMAPS()
{
  cout<<"Skyfields::read_SKYMAPS"<<endl;
 

  string SKYMAPS_filename;

  
  int debug;
  int verbose=1;

  fitsfile *fptr=0;
  int status=0; // need to initialize to 0  or error
  int hdunum;
  int hdutype;
  long nrows;
  int ncols;
  int colnum;
  int casesen=1;
  char   colname[100];
  string colnamestring;
  int datatype;
  long firstrow,firstelem,nelements;
  double *nulval=NULL;
  int    *anynul=NULL;

#define NMAXROWS 5000
  double RA_MIN[NMAXROWS],RA_MAX[NMAXROWS],RA_CEN[NMAXROWS];
  double DE_MIN[NMAXROWS],DE_MAX[NMAXROWS],DE_CEN[NMAXROWS];
  int    SMAPNR[NMAXROWS];

  debug=1;


  //int fits_open_file( fitsfile **fptr, char *filename, int mode, int *status)
  SKYMAPS_filename="SKYMAPS.fits";
  fits_open_file( &fptr, SKYMAPS_filename.c_str(), READONLY, &status);
   

  cout<<"status="<<status<<endl;
  fits_report_error(stdout, status);

  

  hdunum=2;
  fits_movabs_hdu(fptr, hdunum, &hdutype,  &status);
  cout<<"status="<<status<<endl;
  fits_report_error(stdout, status);
  cout<<"hdutype="<<hdutype<<endl;
  cout<<"BINARY_TBL="<<BINARY_TBL<<endl;

  /*  
int fits_get_num_rows(fitsfile *fptr, long *nrows, int *status)
int fits_get_num_cols(fitsfile *fptr, int  *ncols, int *status)

Get the number of rows or columns in the current FITS table. The number of rows is given by the NAXIS2 keyword and the number of columns is given by the TFIELDS keyword in the header of the table.

_______________________________________________________________
int fits_get_colnum(fitsfile *fptr, int casesen, char *template,
                    int *colnum, int *status)
int fits_get_colname(fitsfile *fptr, int casesen, char *template,
                    char *colname, int *colnum, int *status)
  */

  fits_get_num_rows(fptr, &nrows, &status);
  fits_get_num_cols(fptr, &ncols, &status);
  cout<<"nrows="<<nrows<<" ncols="<<ncols<<endl;
  if(nrows>NMAXROWS){cout<<"too many rows for array size "<<NMAXROWS<< " return!"<<endl; ra_min.resize(0); return ;}

  colnamestring="SMAPNR";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  //                             colnamestring.c_str() did not work as argument (but did for fits_open_read above
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;



  //int fits_read_col(fitsfile *fptr, int datatype, int colnum, long firstrow,
  //     long firstelem, long nelements, void *nulval, void *array, 
  //     int *anynul, int *status)

  firstrow =1;
  firstelem=1;
  nelements=nrows;

   datatype=TINT;  
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, SMAPNR, 
		 anynul, &status);

   cout<<"fits_read_col status="<<status<<endl;

   if(verbose==1) for(int i=0;i<nelements;i++)   cout<<SMAPNR[i]<<endl;

   ///////////////////////////////////////////7


  colnamestring="RA_MIN";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;



  //int fits_read_col(fitsfile *fptr, int datatype, int colnum, long firstrow,
  //     long firstelem, long nelements, void *nulval, void *array, 
  //     int *anynul, int *status)

  firstrow =1;
  firstelem=1;
  nelements=nrows;
  datatype=TDOUBLE;

   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, RA_MIN, 
		 anynul, &status);

   cout<<"fits_read_col status="<<status<<endl;

   if(verbose==1) for(int i=0;i<nelements;i++)   cout<<RA_MIN[i]<<endl;

   ////////////////////////////////////
  colnamestring="RA_MAX";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;



  //int fits_read_col(fitsfile *fptr, int datatype, int colnum, long firstrow,
  //     long firstelem, long nelements, void *nulval, void *array, 
  //     int *anynul, int *status)

  firstrow =1;
  firstelem=1;
  nelements=nrows;
  
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, RA_MAX, 
		 anynul, &status);

   cout<<"fits_read_col status="<<status<<endl;

   if(verbose==1) for(int i=0;i<nelements;i++)   cout<<RA_MAX[i]<<endl;

  ////////////////////////////////////
  colnamestring="RA_CEN";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
 
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;



  //int fits_read_col(fitsfile *fptr, int datatype, int colnum, long firstrow,
  //     long firstelem, long nelements, void *nulval, void *array, 
  //     int *anynul, int *status)

  firstrow =1;
  firstelem=1;
  nelements=nrows;
  
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, RA_CEN, 
		 anynul, &status);

   cout<<"fits_read_col status="<<status<<endl;

   if(verbose==1) for(int i=0;i<nelements;i++)   cout<<RA_CEN[i]<<endl;

  
   ///////////////////////////////////////////////////7


  colnamestring="DE_MIN";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;

  
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, DE_MIN, 
		 anynul, &status);

   cout<<"fits_read_col status="<<status<<endl;

   if(verbose==1)for(int i=0;i<nelements;i++)   cout<<DE_MIN[i]<<endl;

   
  colnamestring="DE_MAX";  
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;


  fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, DE_MAX, 
		 anynul, &status);

  cout<<"fits_read_col status="<<status<<endl;


   ///////////////////////////////////////////////////


  colnamestring="DE_CEN";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;

  
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, DE_CEN, 
		 anynul, &status);

   cout<<"fits_read_col status="<<status<<endl;

   if(verbose==1)for(int i=0;i<nelements;i++)   cout<<DE_CEN[i]<<endl;

   

  //////////////////////////////////////////////////////////
if(verbose==1)
  for(int i=0;i<nelements;i++) 
  cout<<"i="<<i
       <<" RA_MIN="<<RA_MIN[i]<<" RA_MAX="<<RA_MAX[i]<<" RA_CEN="<<RA_CEN[i]
       <<" DE_MIN="<<DE_MIN[i]<<" DE_MAX="<<DE_MAX[i]<<" DE_CEN="<<DE_CEN[i]
       <<" SMAPNR="<<SMAPNR[i] <<endl;

 

  cout<<"copy to valarray"<<endl;



  ra_min    .resize(nelements);     
  ra_max    .resize(nelements); 
  ra_cen    .resize(nelements); 
  dec_min   .resize(nelements);
  dec_max   .resize(nelements);
  dec_cen   .resize(nelements); 
  smapnr    .resize(nelements); 
   

  cout<<"ra_min.size()="<<ra_min.size()<<endl;

  for(int i=0;i<nelements;i++)
  {
   ra_min   [i] =RA_MIN   [i];
   ra_max   [i] =RA_MAX   [i];
   ra_cen   [i] =RA_CEN   [i];
   dec_min  [i] =DE_MIN   [i];
   dec_max  [i] =DE_MAX   [i];  
   dec_cen  [i] =DE_CEN   [i];
   smapnr   [i] =SMAPNR   [i];  
   // correct smapnr 180 to 1180: error in SKYMAPS.fits! tile=001180
   if(smapnr[i]==180) smapnr[i]=1180;
  }

if(verbose==1)
  for(int i=0;i<ra_min.size();i++) 
  cout<<"i="<<i
       <<" ra_min="  <<ra_min[i]<<" ra_max=" << ra_max[i] << " ra_cen=" <<ra_cen[i]
       <<" dec_min="<<dec_min[i]<<" dec_max="<<dec_max[i] <<" dec_cen="<<dec_cen[i]
       <<" smapnr="<<smapnr[i] <<endl;
 

  cout<<"end Skyfields::read_SKYMAPS"<<endl;
  return;
}

////////////////////////////////////////////////////////

void Skyfields::get_skyfield(int smapnr_,
                    double  &ra_min_, double  &ra_max_, double  &ra_cen_,
		    double &dec_min_, double &dec_max_, double &dec_cen_,
			     int    &status)
{
  cout<<"Skyfields::get_skyfield"<<endl;
  cout<<"smapnr_="<<smapnr_<<endl;
  status=1;
  int i=-1;
  for(int ii=0; ii<ra_min.size();ii++)
    {
      if (smapnr[ii]==smapnr_)
       { cout<<"found smapnr_ "<<smapnr_<< " ii="<<ii<<endl;
	 i=ii;
         status=0;
       }
    }
				
  ra_min_=ra_min[i];
  ra_max_=ra_max[i];
  ra_cen_=ra_cen[i];

  dec_min_=dec_min[i];
  dec_max_=dec_max[i];
  dec_cen_=dec_cen[i];

  if(i>-1)
  cout<<"i="<<i
       <<" ra_min="  <<ra_min[i]<<" ra_max=" << ra_max[i] << " ra_cen=" <<ra_cen[i]
       <<" dec_min="<<dec_min[i]<<" dec_max="<<dec_max[i] <<" dec_cen="<<dec_cen[i]
       <<" smapnr="<<smapnr[i] <<endl;

  if(i==-1)
    cout<<"smapnr_ not found  "<<smapnr_<<endl;
  
  return;
};

////////////////////////////////////////////////////////////////////
/*
int main()
{
  cout<<"Skyfields.cc"<<endl;
  Skyfields skyfields;

  int smapnr_;
  double ra_min_, ra_max_, ra_cen_;
  double dec_min_,dec_max_,dec_cen_;
  int    status;
  skyfields.read_SKYMAPS();

  smapnr_=180177;
  skyfields.get_skyfield( smapnr_,
                    ra_min_, ra_max_, ra_cen_,
		    dec_min_, dec_max_, dec_cen_,
			  status);

  smapnr_=1801771;
  skyfields.get_skyfield( smapnr_,
                    ra_min_, ra_max_, ra_cen_,
		    dec_min_, dec_max_, dec_cen_,
			  status);

  smapnr_=319171;
  skyfields.get_skyfield( smapnr_,
                    ra_min_, ra_max_, ra_cen_,
		    dec_min_, dec_max_, dec_cen_,
			  status);

}
*/
