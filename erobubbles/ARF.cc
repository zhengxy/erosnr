

using namespace std;
#include<iostream>
#include<cstdio>   
#include<cstring>          
#include<valarray>
#include"fitsio.h"

#include <sstream>
#include <string>

#include"ARF.h"

////////////////////////////////////////////////////////
    
                              
void ARF:: read_ARF  (int verbose)
{
   
  cout<<"read_ARF"<<endl;
 

 

  fitsfile *fptr=0;
  int status=0; // need to initialize to 0  or error
  int hdunum;
  int hdutype;
  long nrows;
  int ncols;
  int colnum;
  int casesen=1;
  char   colname[100];
  string colnamestring;
  int datatype=TFLOAT;
  long firstrow,firstelem,nelements;
  double *nulval=NULL;
  int    *anynul=NULL;



  //int fits_open_file( fitsfile **fptr, char *filename, int mode, int *status)

  

  string event_full_filename="ARF_nocorr.fits";
         event_full_filename="tm1_arf_filter_000101v02.fits";

  // tm  1 2 3 4 6 are equal, tm5 7 differ.
  // for 1 2 3 4 6 use tm1 * 5
	 
  
  cout<<"event_full_filename="<<event_full_filename<<endl;
  status=0;
  fits_open_file( &fptr, event_full_filename.c_str(), READONLY, &status);
   

  cout<<"status="<<status<<endl;
  fits_report_error(stdout, status);

  if(status!=0) {cout<<"error opening file, return"<<endl; return;}

  // first header for check
  
  hdunum=1;  // image
  status=0;
  fits_movabs_hdu(fptr, hdunum, &hdutype,  &status);
  cout<<"status="<<status<<endl;
  fits_report_error(stdout, status);
  cout<<"hdutype="<<hdutype<<endl;
  cout<<"IMAGE_HDU ="<< IMAGE_HDU<<endl;
  cout<<"BINARY_TBL="<<BINARY_TBL<<endl;
 

  // header 2 has table
  
    
   hdunum=2;
   status=0;
   fits_movabs_hdu(fptr, hdunum, &hdutype,  &status);
   cout<<"event list status="<<status<<endl;
   fits_report_error(stdout, status);
   cout<<"event list hdutype="<<hdutype<<endl;
   cout<<"BINARY_TBL="<<BINARY_TBL<<endl;

  status=0;
  fits_get_num_rows(fptr, &nrows, &status);
  fits_get_num_cols(fptr, &ncols, &status);

  cout<<"ARF nrows="<<nrows<<" ncols="<<ncols<<endl;

  int i;
    
  

  float *ARRAY;
         ARRAY=new float[nrows];

  colnamestring="ENERG_LO";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;

  //                             colnamestring.c_str() did not work as argument (but did for fits_open_read above

  status=0;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<" colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;


  firstrow =1;
  firstelem=1;
  nelements=nrows;
   
  
  energ_lo    .resize(nelements);               
  energ_hi    .resize(nelements);
  specresp    .resize(nelements);
  
    
   status=0;
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, ARRAY, 
		 anynul, &status);

   cout<<"ARRAY status="<<status<<endl;

  
   if(verbose==2) for(int i=0;i<nelements;i++)   cout<<"ARRAY="<<ARRAY[i]<<endl;

 
   for(int i=0;i<nelements;i++)   energ_lo    [i]=ARRAY    [i];
  
    
    
  colnamestring="ENERG_HI";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;

   status=0;
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, ARRAY, 
		 anynul, &status);

   cout<<"ARRAY status="<<status<<endl;

   if(verbose==2)for(int i=0;i<nelements;i++)   cout<<"ARRAY="<<ARRAY[i]<<endl;

 
 for(int i=0;i<nelements;i++)   energ_hi   [i]=ARRAY   [i];
  

    if(verbose==2)
  for(int i=0;i<nelements;i++)   cout<<" i="<<i<<" ARRAY="<<ARRAY[i]<<endl;
    
    
  colnamestring="SPECRESP";  // response in cm^2
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;

  status=0;
  fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, ARRAY, 
		 anynul, &status);

  cout<<"ARRAY status="<<status<<endl;

  
     for(int i=0;i<nelements;i++)   specresp[i]=ARRAY[i];  
  
    

  
if(verbose==1||verbose==2)
  for(int i=0;i<nelements;i++)   cout<<"ARF: i="<<i<<" energ_lo="<<energ_lo[i]<<" energ_hi="<<energ_hi[i]<<" keV" <<" specresp="<<specresp[i]<< " cm^2 "<<endl;

    status=0;
    fits_close_file( fptr,  &status);
    cout<<"ARF close file status="<<status<<endl;
    
    delete[] ARRAY;

   
  cout<<"end read_ARF"<<endl;


  
  return;
}

//////////////////////////////////////
void ARF:: print_ARF  (int verbose)
{
   
  cout<<"print_ARF"<<endl;
  if(verbose==1||verbose==2)
   for(int i=0;i<energ_lo.size();i++)
     cout<<" ARF: i="<<i<<" energ_lo="<<energ_lo[i]<<" energ_hi="<<energ_hi[i]
	 << " de="<<energ_hi[i]-energ_lo[i]<<" keV"
       	 << " dloge="<<log(energ_hi[i]/energ_lo[i])
	 <<" specresp="<<specresp[i]<< " cm^2 "<<endl;
   cout<<"end print_ARF"<<endl;
  return;
}
/////////////////////////////////////////////////////////////////////
double ARF::  get_ARF(double energy,int verbose, int &status)
{
 cout<<"get_ARF"<<endl;
 cout<<"energy="<<energy<<endl;
 double specresp_=10;
 status=0;
 
 
 int i;
 int  ii=-1;

 for (i=0;i<energ_lo.size();i++)
   {
     cout<<"i="<<i<<" energy="<<energy<<" lo "<<energ_lo[i] <<" hi "<< energ_hi[i]<<endl;
     if (energy>energ_lo[i] && energy<energ_hi[i]) ii=i;
   }
 
 cout<<"ii="<<ii<<endl;
 
    if(  energy<energ_lo[0]                   )             { ii=0;                 status=-1;cout<<"outside range so set  ii="<<ii<<endl;}
		 
    if(  energy>energ_hi[energ_lo.size()-1 ]  )             { ii=energ_lo.size()-1; status=-1;cout<<"outside range so set  ii="<<ii<<endl;}
 
 
 cout<<"status="<<status<<endl;
 
 specresp_=specresp[ii];
 return specresp_;
}
////////////////////////////////////////////////////////

double ARF:: get_ARF(double energy_min,double energy_max,double spectral_index,int verbose, int &status)
{
   cout<<"get_ARF(energy_min....)"<<endl;
 cout<<"energy_min="<<energy_min<<" energy_max="<<energy_max  <<endl;
 double specresp_,norm;
 status=0;
 
  
 
 int i;
 
 specresp_=0;
 norm     =0;
 double weight;
 double de;
 double e1,e2;
 int select;
 
 /*
                      energy_min                    energy_max
                          |                             |  
                                 energ_lo  energ_hi                              select = 1

           energ_lo                        energ_hi                              select = 2

                                 energ_lo                         energ_hi       select = 3
            
 */


 
 for (i=0;i<energ_lo.size();i++)
 {
  cout<<"before select: i="<<i<<" energy_min="<<energy_min<<" energy_max="<<energy_max
	 <<" lo[i]= "<<energ_lo[i] <<" hi[i] "<< energ_hi[i]<<" specresp[i]="<<specresp[i]
	 <<endl;
  
  e1=e2=0; select=0;
  if (energ_lo[i]>energy_min &&  energ_hi[i]<energy_max)                            {e1=energ_lo[i];  e2=energ_hi[i];select=1;}
  if (energ_lo[i]<energy_min &&  energ_hi[i]>energy_min  && energ_hi[i]<energy_max) {e1=energy_min ;  e2=energ_hi[i];select=2;}
  if (energ_lo[i]>energy_min &&  energ_lo[i]<energy_max  && energ_hi[i]>energy_max) {e1=energ_lo[i];  e2=energy_max ;select=3;}
   
  weight=0;
  if(select>0)
  {
   weight=pow(e1,-spectral_index+1) - pow(e2,-spectral_index+1);
     
   specresp_+=weight*specresp[i];
   norm     +=weight;
  

   cout<<"after select : i="<<i<<" energy_min="<<energy_min<<" energy_max="<<energy_max
      <<" lo[i]= "<<energ_lo[i] <<" hi[i] "<< energ_hi[i]
      <<" select="<<select<<" e1= "<<e1 <<" e2= "<< e2
      <<" specresp[i]="<<specresp[i]
      <<" weight="<<weight
      <<" sum weight*specresp_="<<specresp_<<" sum weight= norm="<<norm
      <<endl;
   
   } // if select
  
   } //for i

 cout<<"after selection, before normalizing: specresp_="<<specresp_<<" norm="<<norm<<endl;
 
 status=0;
 if(norm<=0) status=-1;
 cout<<"status="<<status<<endl;
 
 if(norm>0) specresp_ /= norm;
 
 cout<<"after selection, after normalizing: specresp_="<<specresp_<<endl;

 int num_tms=5;
 specresp_ *= num_tms;
 cout<<"after selection, after scaling tm1 to "<<num_tms<<" tms"<< " : specresp_="<<specresp_<<endl;
 
 return specresp_;

}
/*
////////////////////////////////////////////////////////
int main()
{ 
  cout<<"ARF.cc"<<endl;

  ARF arf;
  int verbose=1;
  arf.read_ARF(verbose);
  arf.print_ARF(verbose);

  double energy=1.1;
  int status;
  double specresp;
  // specresp=arf.get_ARF(energy,verbose,status);
  cout<<"energy="<<energy<<" specresp="<<specresp<<endl;


  double energy_min=1.5;
  double energy_max=2.6;
  double spectral_index=3;
  specresp=arf.get_ARF(energy_min,energy_max,spectral_index ,verbose,status);
  cout<<"energy_min="<<energy_min<<" energy_max="<<energy_max<<" spectral_index="<<spectral_index<<" specresp="<<specresp<<" status="<< status<<endl;
    
  cout<<"===   ARF.cc complete"<<endl;

  return 0;
}
*/
