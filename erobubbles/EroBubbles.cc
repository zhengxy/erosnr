
using namespace std;
#include<cstdio>    
#include<string>    
#include<fstream>   
#include<iostream>
#include<cmath>

#include"EroBubbles.h"
#include "healpix_map.h"
#include "healpix_map_fitsio.h"
#include "arr.h"
#include "fitshandle.h"
#include "pointing.h"  

///////////////////////////////////////////////////////////////
int EroBubbles::init(double r_minN_, double r_maxN_,
                     double r_minS_, double r_maxS_,
                     double xcN_, double ycN_, double zcN_,
                     double xcS_, double ycS_, double zcS_,
                     string RLH_,
                     double ds_,  double s_max_,
                     double R0_)
{
  cout<<"EroBubbles.init(r_min_,r_max_)"<<endl;
 
  

  r_minN=r_minN_;
  r_maxN=r_maxN_;

  r_minS=r_minS_;
  r_maxS=r_maxS_;

  xcN=xcN_;
  ycN=ycN_;
  zcN=zcN_;


  xcS=xcS_;
  ycS=ycS_;
  zcS=zcS_;

  RLH=RLH_;

  ds    = ds_;
  s_max = s_max_;

  R0=R0_;

  return 0;

};

////////////////////////////////////////////////////////////////////
double EroBubbles::emissivity(double x, double y, double z, double E)
{
  int debug=0;
  
if(debug==1)
  cout<<"EroBubbles.emissivity(x,y,z,E)"<<endl;
  
  

  double emissivity_;

  double dx, dy, dz;
  
  if(z>=0.)
  {
   dx=x-xcN;
   dy=y-ycN;
   dz=z-zcN;
  }

  if(z<0.)
  {
   dx=x-xcS;
   dy=y-ycS;
   dz=z-zcS;
  }

  double r=sqrt(dx*dx+dy*dy+dz*dz);
  

  emissivity_=0;

  double emissivity_nominal;
  emissivity_nominal = 1.0e-29 ; // erg cm-3 sr-1 s-1 band 022 from notes

  if(z>=0. && r>r_minN && r<r_maxN)emissivity_ = emissivity_nominal ;

  if(z< 0. && r>r_minS && r<r_maxS)emissivity_ = emissivity_nominal ;

  if(debug==1)
  cout<<"r="<<r<<" emissivity_="<<emissivity_<<endl;
  
return emissivity_ ;

};

/////////////////////////////////////////////////////////////

double EroBubbles::intensity (double l, double b, double E)
{
  int debug=0;

  if(debug==1)
  cout<<"EroBubbles.intensity(l,b,E)"<<endl;
  
  double intensity_;
  
  double x,y,z;
   
  double s;  // line-of-sight distance
  
  double dtr=acos(-1.0)/180.; // degrees to radians pi/180
 
  // RLH: right or left-handed system
  // RHS z=x^y  y decreases with l
  // LHS z=y^x  y increases with l

     
  intensity_=0.;
  s=0.;
  for(s=0; s<=s_max;s+=ds)
  {
   x = R0 - s * cos(b*dtr) * cos(l*dtr);

   if(RLH=="R")
   y =    - s * cos(b*dtr) * sin(l*dtr);
   if(RLH=="L")
   y =      s * cos(b*dtr) * sin(l*dtr);

   z =      s * sin(b*dtr);
  
   intensity_+=emissivity(x,y,z,E);

   if(debug==1)
     cout<<"l="<<l<<" b="<<b<<" s="<<s<<" RLH="<<RLH<<" x="<<x<<" y= "<<y<<" z="<<z
             <<" E="<<E<<" intensity_= "<<intensity_<<endl;
  }
  
  intensity_*=ds;
  double kpctocm;
         kpctocm=3.08e21;
  intensity_*=kpctocm;

  double arcmin2_in_sr=1./pow(57.3*60.,2); 
  intensity_*=arcmin2_in_sr;

  return intensity_ ;

};
//////////////////////////////////////////////////////////////////


             
int EroBubbles::healpix_skymap(int order, string outfile, double E, int debug)
{

  
  cout<<endl<< "=== healpix_skymap"<<endl<<endl;

    
  PDT datatype = PLANCK_FLOAT64; // HealPix defines it this way
      datatype = PLANCK_FLOAT32;
  
 
  arr<double>data; // for HealPix array class
  
  int hdu;
  
  int ncolnum,colnum;
 
 


  //==================== 

          

 
  ncolnum=1;

  cout<<"writing healpix skymap to "<<outfile<<endl;


  fitshandle out;  
  out.create("!"+outfile); // ! to overwrite

  arr<string> colname;
  colname.alloc(ncolnum);

  for(int j=0;j<ncolnum;j++)   colname[j]="bubbles";

  Healpix_Map<double> map_RING_out(order,RING);
  prepare_Healpix_fitsmap(out,map_RING_out, datatype, colname);
  
  out.set_key(string("EXTNAME"),string("SKYMAP"));// extension HDU name
     
  colnum=1;
 
  cout<< "map_RING_out: ";
  cout<<" Npix  = "<<map_RING_out.Npix(); 
  cout<<" Nside = "<<map_RING_out.Nside();
  cout<<" Order = "<<map_RING_out.Order();
  cout<<" Scheme= "<<map_RING_out.Scheme()<<endl; // 0 = RING, 1 = NESTED

  data.alloc(map_RING_out.Npix());
  
  pointing pointing_;
  
  double l,b;
  double rtd=180./acos(-1.); // radians to degrees 180/pi

 E=3; // should be in init

  for (int ipix=0; ipix<map_RING_out.Npix(); ipix++)
  {
   pointing_ = map_RING_out.pix2ang(ipix);

   l =     pointing_.phi  *rtd;
   b = 90.-pointing_.theta*rtd;


   data[ipix] = intensity(l,b,E);   

   if(debug==1)
   cout<<"ipix="<<ipix<<" theta="<<pointing_.theta<<" phi="<<pointing_.phi
       <<" l="<<l<<" b="<<b<<" data[ipix]= "<<data[ipix]<<endl;
  }

  out.write_column(colnum,data); // fitshandle.h

  out.close();                                                         


  // for future use

  map_RING_out.Set(data,RING); // data is zero size after call!

  for (int ipix=0; ipix<map_RING_out.Npix(); ipix++)
  {
   pointing_ = map_RING_out.pix2ang(ipix);

   l =     pointing_.phi  *rtd;
   b = 90.-pointing_.theta*rtd;

   if(debug==1)
   cout<<"ipix="<<ipix<<" theta="<<pointing_.theta<<" phi="<<pointing_.phi
       <<" l="<<l<<" b="<<b<<" map_RING_out[ipix]= "<<map_RING_out[ipix]<<endl;
  }
 

 ////////////////////////////////////////////////////////

  cout<<endl<<" ==== healpix_skymap complete"<<endl;

 return 0;
  
};

