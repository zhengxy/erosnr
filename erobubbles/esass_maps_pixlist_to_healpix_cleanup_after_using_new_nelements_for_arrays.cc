

using namespace std;
#include<iostream>
#include<cstdio>   
#include<cstring>          
#include<valarray>
#include"fitsio.h"

#include "healpix_map.h"
#include "healpix_map_fitsio.h"
#include "arr.h"
#include "fitshandle.h"
#include "pointing.h"  
#include "trafos.h"

#include <sstream>
#include <string>

#include "Skyfields.h"

int read_event_filename_list(string event_filename_list_infile, valarray<string> &event_filename_list,int nline,int debug);

void read_esass_evtool_skymaps(string event_filename,
			       string esass_skymaps_directory,
valarray<double>&ra,valarray<double>&dec,valarray<double>&energy,
valarray<int>&events, valarray<double>&exposure,
valarray<double>&eventlist_ra,valarray<double>&eventlist_dec,valarray<double>&eventlist_energy,
int &SKYFIELD,int debug);

////////////////////////////////////////////////////////




//////////////////////////////////////////////////////////////////////////////////////////

    
  
                          
         
  
/////////////////////////////////////////////////////////
int read_event_filename_list(string event_filename_list_infile, valarray<string> &event_filename_list,int nline,int debug)

{
//based partly on ~/propagate/c/gitlab/galplot/source/Galdef.cc
  cout<<"read_event_filename_list";
  cout<<" event_filename_list_infile="<<event_filename_list_infile;
  cout<<" nline="<<nline;
  cout<<" debug="<<debug<<endl; 
 
  char  filename[200];
  strcpy(filename,event_filename_list_infile.c_str());
  
  FILE *ft;
  ft=fopen(filename,"r");
  if(ft==NULL) {
      cout<<"no input event filename list called "<<filename<<endl; return -1;
  }
 


//int Galdef::read_galdef_parameter(char *filename, char *parstring, char *value)


// char input[37];
   char input[126];
   int stat=0;

// ls -1 e*fits >> input_testfile
// this give 36 chars due to \0 or newline  but want 35 hence need to truncate

//  /afs/ipp-garching.mpg.de/home/a/aws/volume3/erosita/eventlists/eventlist/051/153/EXP_946/eb02_153051_020_EventList_c946.fits  wc:   125 chars but 124 works below
// this gives 124 chars:

   string teststringlength="/afs/ipp-garching.mpg.de/home/a/aws/volume3/erosita/eventlists/eventlist/051/153/EXP_946/eb02_153051_020_EventList_c946.fits";
   cout<<"testing filename string length:"<<teststringlength.size()<<endl;

   // Stroustrup p. 595
   string::size_type ifits;
   ifits=teststringlength.find("fits");
   cout<< "testing position of string fits: ifits="<<ifits<<endl;
   cout<< "hence string length = ifits+4 = "<<ifits+4<<endl;

valarray<string> instring;

instring.resize(nline);

for(int iline=0;iline<nline;iline++)
{

    //https://www.cplusplus.com/reference/cstdio/fgets/
    
 fgets(input,800,ft);
    

 // cout<<"printf:"<<endl;
 // printf("%s",input);       // string is \0 terminated
 
 if(debug==1)cout<<"strlen(input)="<<strlen(input)<<endl;

instring[iline]=input;

 if(debug==1)cout<<"instring= "<<instring[iline]<<endl;

cout<<"iline="<<iline<<" instring="<<instring[iline]<<endl;

 if(debug==1)cout<<"instring length before resize="<<instring[iline].length()<<endl;

 // instring[iline].resize(35);

   
   ifits=instring[iline].find("fits");
   cout<< "testing position of string fits in instring: ifits="<<ifits<<endl;
   
   int instring_length = ifits+4;
   cout<< "hence instring length = ifits+4 = "<<instring_length<<endl;
   
 

 instring[iline].resize(instring_length); 

 if(debug==1)cout<<"instring length after resize="<<instring[iline].length()<<endl;

/*
 cout<<"testing with explicit event file"<<endl;
strcpy(input,"eb01_108051_020_EventList_c946.fits");
cout<<"strcmp "<<strcmp(input,"eb01_108051_020_EventList_c946.fits")<<endl;
*/

  } // iline


 if(debug==1)
 {
 cout<<"---------testing open on instring"<<endl;

for(int iline=0;iline<nline;iline++)
  {
 
    if(debug==1)
    {
     cout<<"iline="<<iline<<" "<<instring[iline]<<endl;
     cout<<"length="<<instring[iline].length()<<endl;
     cout<<"instring="<<instring[iline]<<endl;
     cout<< "c_str=" <<instring[iline].c_str()  <<endl;
     }

 FILE *eventft;
       eventft=fopen(instring[iline].c_str(),"r");

  if(eventft!=NULL) cout<<"opened file "   <<instring[iline]<< " c_str=" <<instring[iline].c_str()<<" eventft="<<eventft  <<endl;

  if(eventft==NULL) cout<<"no file called "<<instring[iline]<< " c_str=" <<instring[iline].c_str()  <<endl;
  
  fclose(eventft);

}
 }// debug==1
  
   fclose(ft); 

   event_filename_list=instring;

   cout<<"number of event filenames read in list="<<event_filename_list.size()<<endl;

   return stat;
}

////////////////////////////////////////////////////////

void read_esass_evtool_skymaps(string event_filename,
                               string esass_skymaps_directory,
valarray<double>&ra,valarray<double>&dec,valarray<double>&energy,
valarray<int>&events, valarray<double>&exposure,
valarray<double>&eventlist_ra,valarray<double>&eventlist_dec,valarray<double>&eventlist_energy,
int &SKYFIELD,int debug)
{
   
  cout<<"read_esass_evtool_skymaps"<<endl; //!!!!!!!!!!!!!!!!!!!!!!
   
  cout<<" event_filename= "<<event_filename<<endl;

  int verbose=0;

  fitsfile *fptr=0;
  int status=0; // need to initialize to 0  or error
  int hdunum;
  int hdutype;
  long nrows;
  int ncols;
  int colnum;
  int casesen=1;
  char   colname[100];
  string colnamestring;
  int datatype=TDOUBLE;
  long firstrow,firstelem,nelements;
  double *nulval=NULL;
  int    *anynul=NULL;
//      was      7000000   8000000 gives coredump
//#define NMAXROWS 7500000

//  float RADECENERGY[NMAXROWS];
//    float *RADECENERGY;
 //   RADECENERGY=new float[NMAXROWS];






  //int fits_open_file( fitsfile **fptr, char *filename, int mode, int *status)

       /*
  string esass_skymaps_directory="/afs/ipp-garching.mpg.de/home/a/aws/volume3/erosita/esass_skymaps/";
         esass_skymaps_directory="/afs/ipp-garching.mpg.de/home/a/aws/volume3/erosita/esass_skymaps/with_eventlists/";
       */
	

  string event_full_filename=esass_skymaps_directory+"evtool_"+event_filename;
  cout<<"event_full_filename="<<event_full_filename<<endl;
  status=0;
  fits_open_file( &fptr, event_full_filename.c_str(), READONLY, &status);
   

  cout<<"status="<<status<<endl;
  fits_report_error(stdout, status);

  if(status!=0) {cout<<"error opening file, return"<<endl; return;}

  hdunum=1;  // image
  status=0;
  fits_movabs_hdu(fptr, hdunum, &hdutype,  &status);
  cout<<"status="<<status<<endl;
  fits_report_error(stdout, status);
  cout<<"hdutype="<<hdutype<<endl;
  cout<<"IMAGE_HDU ="<< IMAGE_HDU<<endl;
  cout<<"BINARY_TBL="<<BINARY_TBL<<endl;

  

  /*  
int fits_get_num_rows(fitsfile *fptr, long *nrows, int *status)
int fits_get_num_cols(fitsfile *fptr, int  *ncols, int *status)

Get the number of rows or columns in the current FITS table. The number of rows is given by the NAXIS2 keyword and the number of columns is given by the TFIELDS keyword in the header of the table.

_______________________________________________________________
int fits_get_colnum(fitsfile *fptr, int casesen, char *template,
                    int *colnum, int *status)
int fits_get_colname(fitsfile *fptr, int casesen, char *template,
                    char *colname, int *colnum, int *status)
  */

  /*
  fits_get_num_rows(fptr, &nrows, &status);
  fits_get_num_cols(fptr, &ncols, &status);
  cout<<"nrows="<<nrows<<" ncols="<<ncols<<endl;
  if(nrows>NMAXROWS){cout<<"too many rows for array size "<<NMAXROWS<< " return!"<<endl; ra.resize(0); return ;}

  colnamestring="RA";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  //                             colnamestring.c_str() did not work as argument (but did for fits_open_read above
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;
  */

  /*
 int fits_read_img / ffgpv
      (fitsfile *fptr, int  datatype, long firstelem, long nelements,
       DTYPE *nulval, > DTYPE *array, int *anynul, int *status)
  */
 
  char comment[400];
  int NAXIS1,NAXIS2;
  double CRPIX1,CRVAL1,CDELT1;
  double CRPIX2,CRVAL2,CDELT2;

   fits_read_key(fptr,   TINT,"NAXIS1" ,&NAXIS1 ,  comment, &status) ; 
   fits_read_key(fptr,   TINT,"NAXIS2" ,&NAXIS2 ,  comment, &status) ; 

   fits_read_key(fptr,TDOUBLE,"CRPIX1" ,&CRPIX1 ,  comment, &status) ;
   fits_read_key(fptr,TDOUBLE,"CRPIX2" ,&CRPIX2 ,  comment, &status) ;

   fits_read_key(fptr,TDOUBLE,"CRVAL1" ,&CRVAL1 ,  comment, &status) ;
   fits_read_key(fptr,TDOUBLE,"CRVAL2" ,&CRVAL2 ,  comment, &status) ;

   fits_read_key(fptr,TDOUBLE,"CDELT1" ,&CDELT1 ,  comment, &status) ;
   fits_read_key(fptr,TDOUBLE,"CDELT2" ,&CDELT2 ,  comment, &status) ;

   cout<<" evtool NAXIS1="<<NAXIS1<<" NAXIS2="<<NAXIS2;
   cout<<" CRPIX1="<<CRPIX1<<" CRVAL1="<<CRVAL1<<" CDELT1="<<CDELT1;
   cout<<" CRPIX2="<<CRPIX2<<" CRVAL2="<<CRVAL2<<" CDELT2="<<CDELT2<<endl;

   status=0;
   fits_read_key(fptr,TINT,"SKYFIELD" ,&SKYFIELD ,  comment, &status) ; 
   cout<<"FITS read SKYFIELD status= "<<status<<" SKYFIELD="<<SKYFIELD<<endl;

// was                  500000
    
#define      NMAXARRAY  400000
//  int  array[NMAXARRAY];
    int *array;
 //        array=new int[NMAXARRAY];

  datatype=TINT;  
  firstelem=1;
  nelements=    NAXIS1 * NAXIS2;

  cout<<"evtool image array: nelements = NAXIS1*NAXIS2 ="<<nelements<<endl;
    
  array=new int[nelements];

  if(nelements>NMAXARRAY) { cout<<"array size NMAXARRAY= "<<NMAXARRAY<<" too small for image, returning"<<endl;  return;}
  
  status=0;
  fits_read_img 
      (fptr, datatype, firstelem, nelements,
       nulval, array, anynul, &status);

    cout<<"evtool fits_read_img status="<<status<<endl;

    fits_report_error(stdout, status);

    int i;
    if(debug==2)
    for(i=0; i<nelements;i++){  cout<<array[i]<<" ";}

    int nevents;
    nevents=0;
    for(i=0; i<nelements;i++) nevents+=array[i];

    cout<<endl<<"read esass_evtool_skymaps: total events in evtool image  = nevents = "<<nevents<<endl;


   
    
    // read event list

   cout<<"---------------read event list"<<endl;
   
   hdunum=2;
   status=0;
   fits_movabs_hdu(fptr, hdunum, &hdutype,  &status);
   cout<<"event list status="<<status<<endl;
   fits_report_error(stdout, status);
   cout<<"event list hdutype="<<hdutype<<endl;
   cout<<"BINARY_TBL="<<BINARY_TBL<<endl;

  status=0;
  fits_get_num_rows(fptr, &nrows, &status);
  fits_get_num_cols(fptr, &ncols, &status);

  cout<<"event list nrows="<<nrows<<" ncols="<<ncols<<endl;

#define NMAXROWS 7500000    
  if(nrows>NMAXROWS){cout<<"too many rows for array size NMAXROWS="<<NMAXROWS<< " return!"<<endl; ra.resize(0); return ;}
    
    
    //      was      7000000   8000000 gives coredump


//  float RADECENERGY[NMAXROWS];
    float *RADECENERGY;
//    RADECENERGY=new float[NMAXROWS];
      RADECENERGY=new float[nrows];

  colnamestring="RA";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;

  //                             colnamestring.c_str() did not work as argument (but did for fits_open_read above

  status=0;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"event list RA colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;


  firstrow =1;
  firstelem=1;
  nelements=nrows;
  
  datatype=TFLOAT;  

    //moved from below
  eventlist_ra    .resize(nelements);               
  eventlist_dec   .resize(nelements);
  eventlist_energy.resize(nelements);
    
  status=0;
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, RADECENERGY, 
		 anynul, &status);

   cout<<"event list RA fits_read_col status="<<status<<endl;

   //verbose=1;
   if(verbose==1) for(int i=0;i<nelements;i++)   cout<<"event list RA="<<RADECENERGY[i]<<endl;

 cout<<"copy RA to event list to valarray eventlist_ra"<<endl;
 for(int i=0;i<nelements;i++)
  {
    eventlist_ra    [i]=RADECENERGY    [i];
  }
    
    
  colnamestring="DEC";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;

   status=0;
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, RADECENERGY, 
		 anynul, &status);

   cout<<"fits_read_col status="<<status<<endl;

   if(verbose==1)for(int i=0;i<nelements;i++)   cout<<"event list DEC="<<RADECENERGY[i]<<endl;

 cout<<"copy DEC to valarray eventlist_dec"<<endl;
 for(int i=0;i<nelements;i++)
  {
    eventlist_dec   [i]=RADECENERGY   [i];
  }

    if(verbose==1)
  for(int i=0;i<nelements;i++)   cout<<"event list DEC i="<<i<<" RADECENERGY="<<RADECENERGY[i]<<endl;
    
    
  colnamestring="PI";  //energy in keV
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;

  status=0;
  fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, RADECENERGY, 
		 anynul, &status);

  cout<<"fits_read_col status="<<status<<endl;

  cout<<"copy ENERGY to valarray eventlist_energy"<<endl;
     for(int i=0;i<nelements;i++)
  {
    eventlist_energy[i]=RADECENERGY[i]*1.0e-3; // eV->keV 
  }
    
if(verbose==1)
  for(int i=0;i<nelements;i++)   cout<<"event list ENERGY i="<<i<<" RADECENERGY="<<RADECENERGY[i]<<endl;

  
if(verbose==1)
  for(int i=0;i<nelements;i++)   cout<<"event list i="<<i<<" ra="<<eventlist_ra[i]<<" dec="<<eventlist_dec[i]<<" energy="<<eventlist_energy[i]<<endl;



 



    status=0;
    fits_close_file( fptr,  &status);// required or error with many datasets
    cout<<"evtool fits_close_file status="<<status<<endl;
    
    delete[] RADECENERGY;

    //////////////////////////////////////////////////////////////////


    // read exposure

  cout<<"--------read exposure"<<endl;

  string exposure_filename = esass_skymaps_directory+"expmap_"+event_filename;
  cout<<"exposure_filename="<<exposure_filename<<endl;
  status=0;
  fits_open_file( &fptr, exposure_filename.c_str(), READONLY, &status);
   

  cout<<"exposure open file status="<<status<<endl;
  fits_report_error(stdout, status);

  

  hdunum=1;  // image
  fits_movabs_hdu(fptr, hdunum, &hdutype,  &status);
  cout<<"status="<<status<<endl;
  fits_report_error(stdout, status);
  cout<<"hdutype="<<hdutype<<endl;
  cout<<"IMAGE_HDU ="<< IMAGE_HDU<<endl;
  cout<<"BINARY_TBL="<<BINARY_TBL<<endl;

  // check image paramters for consistency with evtool

  int NAXIS1_expmap,NAXIS2_expmap;
  double CRPIX1_expmap,CRPIX2_expmap,CRVAL1_expmap,CRVAL2_expmap,CDELT1_expmap,CDELT2_expmap;

   fits_read_key(fptr,   TINT,"NAXIS1" ,&NAXIS1_expmap ,  comment, &status) ; 
   fits_read_key(fptr,   TINT,"NAXIS2" ,&NAXIS2_expmap ,  comment, &status) ; 

   fits_read_key(fptr,TDOUBLE,"CRPIX1" ,&CRPIX1_expmap ,  comment, &status) ;
   fits_read_key(fptr,TDOUBLE,"CRPIX2" ,&CRPIX2_expmap ,  comment, &status) ;

   fits_read_key(fptr,TDOUBLE,"CRVAL1" ,&CRVAL1_expmap ,  comment, &status) ;
   fits_read_key(fptr,TDOUBLE,"CRVAL2" ,&CRVAL2_expmap ,  comment, &status) ;

   fits_read_key(fptr,TDOUBLE,"CDELT1" ,&CDELT1_expmap ,  comment, &status) ;
   fits_read_key(fptr,TDOUBLE,"CDELT2" ,&CDELT2_expmap ,  comment, &status) ;

   cout<<" expmap NAXIS1="<<NAXIS1_expmap<<" NAXIS2="<<NAXIS2_expmap;
   cout<<" CRPIX1="<<CRPIX1_expmap<<" CRVAL1="<<CRVAL1_expmap<<" CDELT1="<<CDELT1_expmap;
   cout<<" CRPIX2="<<CRPIX2_expmap<<" CRVAL2="<<CRVAL2_expmap<<" CDELT2="<<CDELT2_expmap<<endl;

   //NAXIS1_expmap+=1; // test axes check
   cout<<"checking evtool vs expmap NAXIS1 NAXIS2 ";if(NAXIS1_expmap==NAXIS1 && NAXIS2_expmap==NAXIS2) {cout<<"OK!"<<endl;}
   else {cout<<"axes inconsistent!: return from subroutine"<<endl;return;}
  

//  float  exposure_array[NMAXARRAY];  // was double
    float *exposure_array;
//           exposure_array=new float[NMAXARRAY];
    
    

  datatype=TFLOAT;  
  firstelem=1;
  nelements=    NAXIS1 * NAXIS2;  // error if expmap image has different dimensions from evtool!! hence check above

  cout<<"exposure_array nelements="<<nelements<<endl;
  exposure_array=new float[nelements];

  if(nelements>NMAXARRAY) { cout<<"array size "<<NMAXARRAY<<" too small for image, returning"<<endl;  return;}
  
  status=0;
  fits_read_img 
      (fptr, datatype, firstelem, nelements,
       nulval, exposure_array, anynul, &status);

    cout<<"exposure fits_read_img status="<<status<<endl;

    fits_report_error(stdout, status);

   if(debug==2)     for(i=0; i<nelements;i++){  cout<<"exposure_array="<<exposure_array[i]<<" ";}

   double xrefval,yrefval,xrefpix,yrefpix,xinc,yinc,rot;
   char coordtype[5]; // was 4!

   status=0;

    fits_read_img_coord 
      (fptr, &xrefval, &yrefval,
       &xrefpix, &yrefpix, &xinc, &yinc,
       &rot, coordtype, &status);

    cout<<"xrefval="<<xrefval <<" yrefval="<<yrefval<<" xrefpix="<<xrefpix<<" yrefpix="<<yrefpix
        <<" xinc="<<xinc<<" yinc="<<yinc<<" rot="<<rot<<" coordtype="<<coordtype  <<endl;


   double total_exposure;
   total_exposure=0;
    for(i=0; i<nelements;i++) total_exposure+=exposure_array[i];

    cout<<endl<<"read esass_evtool_skymaps: total exposure in expmap image  = "<<total_exposure<<endl;

    fits_close_file( fptr,  &status); //required or error for many datasets
    cout<<"fits_close_file status="<<status<<endl;

    //////////////////////////////////////

    cout<<"before ra.resize:";
    cout<<" ra.size()="<<ra.size()<<endl;

    ra      .resize(nelements); cout<<"ra.size()        ="<<ra.size()<<endl;            
    dec     .resize(nelements); cout<<"dec.size()       ="<<dec.size()<<endl;
    energy  .resize(nelements); cout<<"energy.size()    ="<<energy.size()<<endl;
    events  .resize(nelements); cout<<"events.size()    ="<<events.size()<<endl;
    exposure.resize(nelements); cout<<"exposure.size()  ="<<exposure.size()<<endl;
    
    i=0;
    double rapix,decpix;
    double xpix,ypix;

    for(int iy=0; iy<NAXIS2;iy++)
    for(int ix=0; ix<NAXIS1;ix++)
    {  
 
      // Mercator values for reference, wrong, not used
      rapix=CRVAL1 + (ix-CRPIX1)*CDELT1;
      if(rapix > 360.)rapix -= 360.;
      if(rapix <   0.)rapix += 360.;

      decpix=CRVAL2 + (iy-CRPIX2)*CDELT2;

    

      xpix=ix+1; // in units of pixels starting at 1
      ypix=iy+1; // in units of pixels starting at 1

      if(debug==2)
      { 
       cout<<"debug="<<debug;
       cout<<" ix="<<ix<< " iy="<<iy;
       cout<<" i="<<i<<" array="<<array[i];
       cout<<" rapix="<<rapix<<" decpix="<<decpix;
       cout<<" xpix="  <<xpix<<" ypix="    <<ypix<<endl;
      }      

      
      
      double xpos,ypos;

      status=0; // needed to avoid possible error

      fits_pix_to_world 
      (xpix, ypix, xrefval, yrefval,
       xrefpix, yrefpix, xinc, yinc,
       rot, coordtype, &xpos, &ypos,
       &status);
   
	       ra[i]   = xpos;
	      dec[i]   = ypos;
	   energy[i]   = 1.0;// fake
 	   events[i]   =          array[i];
	 exposure[i]   = exposure_array[i];  
 
	 debug=3;
         if(status!=0)
	 {
	 if (debug==3)  cout<<" coords "<<event_filename<< " i="<<i<<" ix="<<ix<< " iy="<<iy
                            <<" ra="<<ra[i]<<" dec="<<dec[i] <<"  events="<<events[i] <<"  exposure="<<exposure[i] <<endl;
         if (debug==3)  cout<<" mercator coords rapix="<<rapix<<" decpix="<<decpix<<endl;
	 if (debug==3)  cout<<" coords i="<<i<<" xpix="<<xpix<<" ypix="<<ypix <<" ra  xpos="<<xpos <<"  dec ypos="<<ypos <<endl;
	 if (debug==3)  cout<<" coords xrefval="<<xrefval <<" yrefval="<<yrefval<<" xrefpix="<<xrefpix<<" yrefpix="<<yrefpix
			    <<" xinc="<<xinc<<" yinc="<<yinc<<" rot="<<rot<<" coordtype="<<coordtype<<" status="<<status <<" dont select"  <<endl;
	 }

         if(status!=0) {ra[i]=-999; dec[i]=-999;}  // flag undefined pixels

      i++;

    } // for iy ix

    cout<<"i after event pixel list generation="<<i<<" should equal nelements="<<nelements<<endl;


    // use orthographic projection
    /*
 int fits_read_img_coord / ffgics
      (fitsfile *fptr, > double *xrefval, double *yrefval,
       double *xrefpix, double *yrefpix, double *xinc, double *yinc,
       double *rot, char *coordtype, int *status)




3
    Calculate the celestial coordinate corresponding to the input X and Y pixel location in the image. 

  int fits_pix_to_world / ffwldp
      (double xpix, double ypix, double xrefval, double yrefval,
       double xrefpix, double yrefpix, double xinc, double yinc,
       double rot, char *coordtype, > double *xpos, double *ypos,
       int *status)

4
    Calculate the X and Y pixel location corresponding to the input celestial coordinate in the image. 

  int fits_world_to_pix / ffxypx
      (double xpos, double ypos, double xrefval, double yrefval,
       double xrefpix, double yrefpix, double xinc, double yinc,
       double rot, char *coordtype, > double *xpix, double *ypix,
       int *status)
    */

  delete[]array;
  delete[]exposure_array;

  cout<<"end read_esass_evtool_skymaps"<<endl;
  return;
}




////////////////////////////////////////////////////////

int esass_skymaps_to_healpix
( valarray<string> event_filename_list,
  string esass_skymaps_directory,
  int order, int coordsys,string outfile, double Emin, double Emax, int debug)
{

  
  cout<<endl<< "=== esass_skymaps_to_healpix"<<endl<<endl;

  

  Skyfields skyfields;

  int smapnr_;
  double ra_min_, ra_max_, ra_cen_;
  double dec_min_,dec_max_,dec_cen_;
  int    status;

  skyfields.read_SKYMAPS();

  // test 
  smapnr_=1180;
  skyfields.get_skyfield( smapnr_,
                    ra_min_, ra_max_, ra_cen_,
		    dec_min_, dec_max_, dec_cen_,
			  status);

   cout<<" test from skyfields.get_skyfield:"
       <<" ra_min="  <<ra_min_<<" ra_max=" << ra_max_ << " ra_cen=" <<ra_cen_
       <<" dec_min="<<dec_min_<<" dec_max="<<dec_max_ <<" dec_cen="<<dec_cen_
       <<" smapnr="<<smapnr_ <<" status="<<status<<endl;

   
    
  PDT datatype = PLANCK_FLOAT64; // HealPix defines it this way
      datatype = PLANCK_FLOAT32;
  
 
  arr<double> data; // for HealPix array class
  arr<double> data_events,data_exposure,data_intensity1,data_intensity2;  
  arr<double> data_events_from_list,data_intensity_from_list;

  int hdu;
  
  int ncolnum,colnum;
 
 
  valarray<double> ra,dec,energy;
  valarray<int>events;
  valarray<double>exposure;
  valarray<double> eventlist_ra,eventlist_dec,eventlist_energy;

  int SKYFIELD;
  int ipix;
  double total_events_selected;

  int intile,outtile;
  int usetile;
      usetile=1;// 1=only in tile 2=only out of tile 3=both
  int useevent;

  int map_type=2; //1=events 2=exposure 3=intensity1  4=intensity2

  //==================== 

  ncolnum=1;

  cout<<"output healpix events skymap= "<<outfile<<endl;
  cout<<"healpix order="<<order<<endl;
  cout<<"healpix coordsys="<<coordsys;
  if(coordsys==1) cout<< " Equatorial";
  if(coordsys==2) cout<< " Galactic"<<endl;
  if(coordsys >2) {cout<<" invalid coordsys"<<endl; return 1;}
  cout<<"Emin="<<Emin<<" Emax="<<Emax<<" keV"<<endl;
  cout<<"debug="<<debug<<endl;

  fitshandle out;  
  out.create("!"+outfile); // ! to overwrite

  fitshandle out1,out2,out3,out4,out5,out6;
  
  out1.create("!events_"             +outfile); // ! to overwrite
  out2.create("!exposure_"           +outfile); // ! to overwrite
  out3.create("!intensity1_"         +outfile); // ! to overwrite
  out4.create("!intensity2_"         +outfile); // ! to overwrite
  out5.create("!events_from_list_"   +outfile); // ! to overwrite
  out6.create("!intensity_from_list_"+outfile); // ! to overwrite


  arr<string> colname;
  colname.alloc(ncolnum);

  for(int j=0;j<ncolnum;j++)   colname[j]="events";

  Healpix_Map<double> map_RING_out(order,RING);
  prepare_Healpix_fitsmap(out,map_RING_out, datatype, colname);
  
    
  Healpix_Map<double>          map_RING_out1(order,RING);
  prepare_Healpix_fitsmap(out1,map_RING_out1, datatype, colname);

  Healpix_Map<double>          map_RING_out2(order,RING);
  prepare_Healpix_fitsmap(out2,map_RING_out2, datatype, colname);

  Healpix_Map<double>          map_RING_out3(order,RING);
  prepare_Healpix_fitsmap(out3,map_RING_out3, datatype, colname);

  Healpix_Map<double>          map_RING_out4(order,RING);
  prepare_Healpix_fitsmap(out4,map_RING_out4, datatype, colname);

  Healpix_Map<double>          map_RING_out5(order,RING);
  prepare_Healpix_fitsmap(out5,map_RING_out5, datatype, colname);

  Healpix_Map<double>          map_RING_out6(order,RING);
  prepare_Healpix_fitsmap(out6,map_RING_out6, datatype, colname);


  out.set_key (string("EXTNAME"),string("SKYMAP"));// extension HDU name

  out1.set_key(string("EXTNAME"),string("SKYMAP"));// extension HDU name
  out2.set_key(string("EXTNAME"),string("SKYMAP"));// extension HDU name
  out3.set_key(string("EXTNAME"),string("SKYMAP"));// extension HDU name
  out4.set_key(string("EXTNAME"),string("SKYMAP"));// extension HDU name
  out5.set_key(string("EXTNAME"),string("SKYMAP"));// extension HDU name
  out6.set_key(string("EXTNAME"),string("SKYMAP"));// extension HDU name

     
  colnum=1;
 
  cout<< "map_RING_out: ";
  cout<<" Npix  = "<<map_RING_out.Npix(); 
  cout<<" Nside = "<<map_RING_out.Nside();
  cout<<" Order = "<<map_RING_out.Order();
  cout<<" Scheme= "<<map_RING_out.Scheme()<<endl; // 0 = RING, 1 = NESTED

  data                    .alloc(map_RING_out.Npix());
  data_events             .alloc(map_RING_out.Npix());
  data_exposure           .alloc(map_RING_out.Npix());
  data_intensity1         .alloc(map_RING_out.Npix());
  data_intensity2         .alloc(map_RING_out.Npix());
  data_events_from_list   .alloc(map_RING_out.Npix());
  data_intensity_from_list.alloc(map_RING_out.Npix());

  
  pointing pointing_;
  pointing pointing_galactic;
  
  double l,b;
  double rtd=180./acos(-1.); // radians to degrees 180/pi
  double dtr=acos(-1.)/180.; // degrees to radians pi/180

  double iepoch=2000.;
  double oepoch=2000.;
  Trafo trafo(iepoch,oepoch,Equatorial,Galactic); // cxxsupport  trafos.cc trafos.h
  rotmatrix rm = trafo.Matrix();



  for(ipix=0;ipix<map_RING_out.Npix();ipix++) data                    [ipix]=0;
  for(ipix=0;ipix<map_RING_out.Npix();ipix++) data_events             [ipix]=0;
  for(ipix=0;ipix<map_RING_out.Npix();ipix++) data_exposure           [ipix]=0;
  for(ipix=0;ipix<map_RING_out.Npix();ipix++) data_intensity1         [ipix]=0;
  for(ipix=0;ipix<map_RING_out.Npix();ipix++) data_intensity2         [ipix]=0;
  for(ipix=0;ipix<map_RING_out.Npix();ipix++) data_events_from_list   [ipix]=0;
  for(ipix=0;ipix<map_RING_out.Npix();ipix++) data_intensity_from_list[ipix]=0;


  for(int ieventfile=0;ieventfile<event_filename_list.size();ieventfile++)
  {
    cout<<"esass_maps_to_healpix: ieventfile="<<ieventfile<<endl;

    

    //   read_esass_evtool_skymaps(event_filename_list[ieventfile],ra,dec,energy,events,exposure,SKYFIELD,debug);

   cout<<"before read_esass_evtool_skymaps";
   cout<<" event_filename_list[ieventfile]="<<event_filename_list[ieventfile]<<" esass_skymaps_directory="<<esass_skymaps_directory<<endl;
      
   read_esass_evtool_skymaps(event_filename_list[ieventfile],
                             esass_skymaps_directory,
                                       ra,dec,energy,events,exposure,
			     eventlist_ra,eventlist_dec,eventlist_energy,
   SKYFIELD,debug);
      
   cout<<"after read_esass_evtool_skymaps"<<endl;   

  smapnr_=SKYFIELD;
  skyfields.get_skyfield( smapnr_,
                    ra_min_, ra_max_, ra_cen_,
		    dec_min_, dec_max_, dec_cen_,
			  status);

    cout<<" from skyfields.get_skyfield:"
       <<" ra_min="  <<ra_min_<<" ra_max=" << ra_max_ << " ra_cen=" <<ra_cen_
       <<" dec_min="<<dec_min_<<" dec_max="<<dec_max_ <<" dec_cen="<<dec_cen_
       <<" smapnr="<<smapnr_ <<endl;

    

  cout<<"esass_maps_to_healpix: ra.size()="<<ra.size()<<endl;
  if(ra.size()==0) {cout<<"ra.size=0 signals too many events in file, return 1"<<endl; return 1;}

  intile =0;
  outtile=0;

  int nexposure_used=0;
  
  int events_selected=0;

 

  for(int i=0;i<ra.size();i++) 
  {  
 
    //   if(energy[i] >= Emin && energy[i] <= Emax)   // not valid for pixel list  AWS20210525

   {

     //     debug=1;
     if(debug==1)cout<<" testing pixel i="<<i
         <<" ra_min="  <<ra_min_<<" ra="<<ra[i]<<" ra_max=" << ra_max_ << " ra_cen=" <<ra_cen_
	 <<" dec_min="<<dec_min_<<" dec="<<dec[i]<<" dec_max="<<dec_max_ <<" dec_cen="<<dec_cen_
         <<" smapnr="<<smapnr_ ;

     useevent=0;

     if(     ra[i] >=  ra_min_ &&  ra[i] <= ra_max_
         && dec[i] >= dec_min_ && dec[i] <=dec_max_)
	
       {  
	 if(debug==1) cout<<" testing pixel: is in  tile"    <<endl;
	if(usetile==1|| usetile==3) useevent=1;
        intile+=events[i];
       }

       else 

       {
	 if(debug==1)cout<<" testing pixel: is not in  tile"<<endl;
	if(usetile==2||usetile==3) useevent=1;
        outtile+=events[i];
       }

     if(debug==1)     cout<<"useevent= "<<useevent<<endl;


     //     useevent=1;// using event always for testing
   


  if(useevent==1)
  {

   if(dec[i]<-90.)dec[i]=-90; // problem from image coords
   if(dec[i]>+90.)dec[i]=+90; // problem from image coords

   pointing_.phi   =       ra[i] *dtr;
   pointing_.theta = (90.-dec[i])*dtr; 

   // trafos.h
   /*! Transforms the pointing \a ptg and returns the result. */
   //    pointing operator() (const pointing &ptg) const;

   pointing_galactic=trafo(pointing_);
   l =     pointing_galactic.phi   * rtd;
   b = 90.-pointing_galactic.theta * rtd;

   //if(debug==0&&ieventfile==47)     cout<<"i="<<i<<" ra="<<ra[i]<<" dec="<<dec[i]<<" energy="<<energy[i]       <<" pointing_galactic.phi="<<pointing_galactic.phi<<" pointing_galactic.theta="<<pointing_galactic.theta       <<" l="<<l<<" b="<<b       <<" ipix="<<ipix<<" data[ipix]="<<data[ipix]<<endl; // bug dec<-90


   if(coordsys==1)
   ipix=map_RING_out.ang2pix(pointing_);

  if(coordsys==2)
   ipix=map_RING_out.ang2pix(pointing_galactic);

   double min_exposure_used = 1;//0.01;//1;//10.; // sec
   if(exposure[i]>=min_exposure_used) nexposure_used++;

   
   

   if (exposure[i]>=min_exposure_used) events_selected+=events[i];

   

   if(exposure[i]>=min_exposure_used)
   {
    data_events    [ipix] += events  [i];
    data_exposure  [ipix] += exposure[i];
    data_intensity1[ipix] += events  [i] / exposure[i];
   }


   //debug=2;
   if(debug==2)
     cout<<"esass_to_healpix debug="<<debug<<" i="<<i<<" ra="<<ra[i]<<" dec="<<dec[i]<<" energy="<<energy[i]
       <<" events="<<events[i]<<" exposure="<<exposure[i]
       <<" pointing_galactic.phi="<<pointing_galactic.phi<<" pointing_galactic.theta="<<pointing_galactic.theta
       <<" l="<<l<<" b="<<b
       <<" ipix="<<ipix<<" data[ipix]="<<data[ipix]
       <<" data_events[ipix]="<<data_events[ipix]<<" data_exposure[ipix]="<<data_exposure[ipix]
       <<" data_intensity1[ipix]="<<data_intensity1[ipix]<<endl;
 

     } // useevent==1
    } // if energy
   }  // event pixel

  cout<<"ieventfile="<<ieventfile<< " number of events selected in tile="<<intile<< " out of tile="<<outtile<< " total="<<intile+outtile;
  cout<<" events selected in data using min exposure and tile="<<events_selected<<endl; 
  cout<<"nexposure_used="<<nexposure_used<<endl;


  /////////////////// events from list

   intile=0;
  outtile=0;

  int events_selected_from_list=0;

  for(int i=0;i<eventlist_ra.size();i++) 
  {  
 
   if(eventlist_energy[i] >= Emin && eventlist_energy[i] <= Emax)

   {

     //     debug=1;
     if(debug==1)cout<<" testing events from list  i="<<i
         <<" ra_min="  <<ra_min_<<" ra="<<ra[i]<<" ra_max=" << ra_max_ << " ra_cen=" <<ra_cen_
	 <<" dec_min="<<dec_min_<<" dec="<<dec[i]<<" dec_max="<<dec_max_ <<" dec_cen="<<dec_cen_
         <<" smapnr="<<smapnr_ ;

     useevent=0;

     if(     eventlist_ra[i]  >=  ra_min_ &&  eventlist_ra[i] <= ra_max_
         &&  eventlist_dec[i] >= dec_min_ && eventlist_dec[i] <=dec_max_)
	
       {  
	 if(debug==1) cout<<" testing event from list: is in  tile"    <<endl;
	if(usetile==1|| usetile==3) useevent=1;
        intile++;
       }

       else 

       {
	 if(debug==1)cout<<" testing event from list: is not in  tile"<<endl;
	if(usetile==2||usetile==3) useevent=1;
        outtile++;
       }


   if(debug==1)     cout<<"useevent= "<<useevent<<endl;


     //     useevent=1;// using event always for testing
   


  if(useevent==1)
  {

    //   if(dec[i]<-90.)dec[i]=-90; // problem from image coords
    //   if(dec[i]>+90.)dec[i]=+90; // problem from image coords

   pointing_.phi   =       eventlist_ra [i] *dtr;
   pointing_.theta =  (90.-eventlist_dec[i])*dtr; 

   // trafos.h
   /*! Transforms the pointing \a ptg and returns the result. */
   //    pointing operator() (const pointing &ptg) const;

  pointing_galactic=trafo(pointing_);
   l =     pointing_galactic.phi   * rtd;
   b = 90.-pointing_galactic.theta * rtd;

  


   if(coordsys==1)
   ipix=map_RING_out.ang2pix(pointing_);

   if(coordsys==2)
   ipix=map_RING_out.ang2pix(pointing_galactic);


      
    data_events_from_list    [ipix] ++;
   
    events_selected_from_list++;


    //debug=2;
   if(debug==2)
     cout<<"esass_to_healpix events from list debug ="<<debug<<" i="<<i<<" ra="<<eventlist_ra[i]<<" dec="<<eventlist_dec[i]<<" energy="<<eventlist_energy[i]
     
       <<" pointing_galactic.phi="<<pointing_galactic.phi<<" pointing_galactic.theta="<<pointing_galactic.theta
       <<" l="<<l<<" b="<<b
	 <<" ipix="<<ipix<<" data_events_from_list[ipix]="<<data_events_from_list[ipix]<<endl;
 

     } // useevent==1
    } // if energy
   }  // events from list

  cout<<"ieventfile="<<ieventfile<< " number of events from list selected in tile="<<intile<< " out of tile="<<outtile<< " total="<<intile+outtile;
  cout<<" events selected from list not using min exposure and tile="<<events_selected_from_list<<endl; 
  


  } // eventfile


  for(ipix=0;ipix<map_RING_out.Npix();ipix++) if(data_exposure[ipix]>0) data_intensity2[ipix] = data_events[ipix]/data_exposure[ipix];

  for(ipix=0;ipix<map_RING_out.Npix();ipix++) if(data_exposure[ipix]>0) data_intensity_from_list[ipix] = data_events_from_list[ipix]/data_exposure[ipix];


  total_events_selected=0;
  for(ipix=0;ipix<map_RING_out.Npix();ipix++) total_events_selected+=data_events[ipix];
  cout<<" esass_maps_to_healpix: total events selected in pixel data using min exposure and tile="<<total_events_selected<<endl; 

  double total_events_selected_from_list=0;
  for(ipix=0;ipix<map_RING_out.Npix();ipix++) total_events_selected_from_list+=data_events_from_list[ipix];
  cout<<" esass_maps_to_healpix: total events selected from list in  tile="<<total_events_selected_from_list<<endl; 




  ///////////////////////////////////////////////////////////// output maps

  cout<<"writing healpix skymap to "<< outfile << endl;
  

  

  total_events_selected=0;
  for(ipix=0;ipix<map_RING_out1.Npix();ipix++) total_events_selected+=data_events[ipix];
  cout<<"total events  selected in  healpix map from pixels     ="<<total_events_selected<<endl; 

  total_events_selected=0;
  for(ipix=0;ipix<map_RING_out1.Npix();ipix++) total_events_selected+=data_events_from_list[ipix];
  cout<<"total events  selected in  healpix map from event list = "<<total_events_selected<<endl; 


   out1.write_column(colnum,data_events);     // fitshandle.h
   out2.write_column(colnum,data_exposure);   // fitshandle.h
   out3.write_column(colnum,data_intensity1); // fitshandle.h
   out4.write_column(colnum,data_intensity2); // fitshandle.h
   out5.write_column(colnum,data_events_from_list); // fitshandle.h
   out6.write_column(colnum,data_intensity_from_list); // fitshandle.h

   out. close();
   out1.close();
   out2.close();                                                         
   out3.close();
   out4.close();
   out5.close();
   out6.close();



 ////////////////////////////////////////////////////////

  cout<<endl<<" ==== esass_skymaps_to_healpix complete"<<endl;

 return 0;
  
};

////////////////////////////////////////////////////////
void coord_test()
{
  cout<<"coord_test"<<endl;
  double xpix,ypix,xrefval,yrefval,xrefpix,yrefpix,xinc,yinc,rot,xpos,ypos;
  char coordtype[5];// was 4!
  int status;
  strcpy(coordtype,"-SIN");


  xpix=100;
  ypix=100;

  xrefval=90;
  yrefval=10;

  xrefpix=100;
  yrefpix=100;

  xinc=.1;
  yinc=.1;

  rot=0;

  status=0; // required or can get wrong result

      fits_pix_to_world 
      (xpix, ypix, xrefval, yrefval,
       xrefpix, yrefpix, xinc, yinc,
       rot, coordtype, &xpos, &ypos,
       &status);
   
	  
        
      cout<<"fits_pix_to_world: xpix="<<xpix<<" ypix="<<ypix <<"  xpos="<<xpos <<"   ypos="<<ypos <<endl;
      cout<<"  xrefval="<<xrefval <<" yrefval="<<yrefval<<" xrefpix="<<xrefpix<<" yrefpix="<<yrefpix
	  <<" xinc="<<xinc<<" yinc="<<yinc<<" rot="<<rot<<" coordtype="<<coordtype<<" status="<<status  <<endl<<endl;
	 
      status=0;

    fits_world_to_pix 
      (xpos, ypos, xrefval, yrefval,
       xrefpix, yrefpix, xinc, yinc,
       rot, coordtype, &xpix, &ypix,
       &status);
   
	  
        
      cout<<" result input to fits_world_to_pix: xpix="<<xpix<<" ypix="<<ypix <<"  xpos="<<xpos <<"   ypos="<<ypos <<endl;
      cout<<"  xrefval="<<xrefval <<" yrefval="<<yrefval<<" xrefpix="<<xrefpix<<" yrefpix="<<yrefpix
	  <<" xinc="<<xinc<<" yinc="<<yinc<<" rot="<<rot<<" coordtype="<<coordtype<<" status="<<status  <<endl<<endl;
	



 
      xpos=xrefval;
      ypos=yrefval;

      status=0;

 fits_world_to_pix 
      (xpos, ypos, xrefval, yrefval,
       xrefpix, yrefpix, xinc, yinc,
       rot, coordtype, &xpix, &ypix,
       &status);
   
	  
        
      cout<<" fits_world_to_pix: xpix="<<xpix<<" ypix="<<ypix <<"  xpos="<<xpos <<"   ypos="<<ypos <<endl;
      cout<<"  xrefval="<<xrefval <<" yrefval="<<yrefval<<" xrefpix="<<xrefpix<<" yrefpix="<<yrefpix
	  <<" xinc="<<xinc<<" yinc="<<yinc<<" rot="<<rot<<" coordtype="<<coordtype<<" status="<<status  <<endl<<endl;


      xrefval=0;
      yrefval=-90;
      xpos =90;
      ypos=-85;

      

      for(xpos=0;xpos<360;xpos+=10)
      {

	status=0;

      fits_world_to_pix 
      (xpos, ypos, xrefval, yrefval,
       xrefpix, yrefpix, xinc, yinc,
       rot, coordtype, &xpix, &ypix,
       &status);
   
	  
        
      cout<<" fits_world_to_pix: xpix="<<xpix<<" ypix="<<ypix <<"  xpos="<<xpos <<"   ypos="<<ypos <<endl;
      cout<<"  xrefval="<<xrefval <<" yrefval="<<yrefval<<" xrefpix="<<xrefpix<<" yrefpix="<<yrefpix
	  <<" xinc="<<xinc<<" yinc="<<yinc<<" rot="<<rot<<" coordtype="<<coordtype<<" status="<<status  <<endl<<endl;
	}



      for(xpix=1;xpix<1000;xpix+=100)
	for(ypix=1;ypix<1000;ypix+=100)
	{

 status=0; // required or can get wrong result

      fits_pix_to_world 
      (xpix, ypix, xrefval, yrefval,
       xrefpix, yrefpix, xinc, yinc,
       rot, coordtype, &xpos, &ypos,
       &status);
   
	  
        
      cout<<"fits_pix_to_world: xpix="<<xpix<<" ypix="<<ypix <<"  xpos="<<xpos <<"   ypos="<<ypos <<endl;
      cout<<"  xrefval="<<xrefval <<" yrefval="<<yrefval<<" xrefpix="<<xrefpix<<" yrefpix="<<yrefpix
	  <<" xinc="<<xinc<<" yinc="<<yinc<<" rot="<<rot<<" coordtype="<<coordtype<<" status="<<status  <<endl<<endl;

	}


  return;
};
////////////////////////////////////////////////////////
int main()
{ 
  cout<<"esass_maps_pixlist_to_healpix_cleanup.cc"<<endl;

//  coord_test(); // return 0;

  string event_filename;
  valarray<double>ra,dec,energy;
  int SKYFIELD;

  valarray<string> event_filename_list;
  string healpix_event_filename;
  int coordsys;
  int order;
  double Emin,Emax;
  int debug;
  

  string event_filename_list_infile; 
  int nline;
  
  int testfile;
  testfile=123;//1;//123;//3;//9;//358141;//9;//207159;//9;//207159;//9;//47090;//9;//47090;//1180;
  

  if(testfile==9)     {event_filename_list_infile="input_testfile9";       nline=4878;} // flat list erass1+2 reference

  if(testfile== 47090){event_filename_list_infile="input_testfile_047090"; nline=1;} // test eb01_047090_020_EventList_c946.fits
  if(testfile==  1180){event_filename_list_infile="input_testfile_001180"; nline=1;} // test em01_001180_020_EventList_c946.fits
  if(testfile==207159){event_filename_list_infile="input_testfile_207159"; nline=1;}//  em02_207159_020_EventList_c946.fits with eventlist 
  if(testfile==358141){event_filename_list_infile="input_testfile_358141"; nline=1;}//  em02_358141_020_EventList_c946.fits test dim check

  if(testfile==  1   ){event_filename_list_infile="eventfile_list_erass1";          nline=2439;}//   erass1
  if(testfile==  3   ){event_filename_list_infile="eventfile_list_erass3";          nline=2439;}//   erass3 
  if(testfile==123   ){event_filename_list_infile="eventfile_list_erass123";        nline=7317;}//   erass1+2+3



   string esass_skymaps_directory="/afs/ipp-garching.mpg.de/home/a/aws/volume3/erosita/esass_skymaps/with_eventlists/";
   // esass_skymaps_directory="/afs/ipp-garching.mpg.de/home/a/aws/volume3/erosita/esass_skymaps/";
   //    esass_skymaps_directory="/afs/ipp-garching.mpg.de/home/a/aws/volume3/erosita/esass_skymaps/test_dimension_check/";
	 esass_skymaps_directory="/afs/ipp-garching.mpg.de/home/a/aws/volume3/erosita/esass_skymaps/test_dimension_check/inconsistent/";
	 esass_skymaps_directory="/afs/ipp-garching.mpg.de/home/a/aws/volume3/erosita/esass_skymaps/with_eventlists_E_0.2_10/";
    
    // sciserver
     esass_skymaps_directory="Storage/aws/persistent/skymaps/with_eventlists_E_0.2_10/";


  debug=0;

  /* moved to later 20210721
  read_event_filename_list(event_filename_list_infile,event_filename_list,nline,debug);

  cout<<"event_filename_list.size()= "<<event_filename_list.size()<<endl;
  */

 /////////////


 order   =9;   //9;//10;//9;
 coordsys=2;   // 1=equatorial, 2=Galactic


 healpix_event_filename="test_cleanup_list9_E_0.5_3.fits";
 healpix_event_filename="test_cleanup_check_dimension_test.fits"; // for directory esass_skymps with inconsistent evtool expmap dim check
 healpix_event_filename="test_cleanup_E_.2_10_no_energy_selection_on_pixmap.fits";
 healpix_event_filename="test_cleanup_E_0.3_0.8.fits";
 healpix_event_filename="test_cleanup_E_0.8_1.5.fits";
 healpix_event_filename="test_cleanup_E_1.5_3.0.fits";
 healpix_event_filename="test_cleanup_E_3.0_8.0.fits";
 healpix_event_filename="test_cleanup_E_0.3_1.5.fits";

 if(testfile==  1)healpix_event_filename="test_cleanup_E_.2_10_erass1.fits";
 if(testfile==  3)healpix_event_filename="test_cleanup_E_.2_10_erass3_prelim.fits";
 if(testfile==123)healpix_event_filename="test_cleanup_E_.2_10_erass123.fits";

 Emin=0.3;
 Emin=0.5;

 Emax=2.3;
 Emax=1.0;
 Emax=3.0;

 Emin=0.2; Emax=10.0;
 Emin=0.3; Emax= 0.8;
 Emin=0.8; Emax= 1.5;
 Emin=1.5; Emax= 3.0;
 Emin=3.0; Emax= 8.0;
 Emin=0.3; Emax= 1.5;

 Emin=0.2; Emax=10.0;

 // for Nicola
 int nicola=123;
     nicola=123;
     nicola=1;
     

 if(nicola==1)
 {

 event_filename_list_infile="eventfile_list_erass1";          nline=2439;//   erass1
 Emin=0.613; Emax=0.693;
 order=7;                                                                          // changed from 8 for compatibility  20210720
 healpix_event_filename="healpix_for_nicola_E_.613_.693_erass1_order_7_testnew.fits";      // changed from 8 for compatibility  20210720
 cout<<"nicola="<<nicola<< " event_filename_list_infile="<<event_filename_list_infile<<" nline="<<nline<<endl;

 }

 if(nicola==123)
 {
 event_filename_list_infile="eventfile_list_erass123";       nline=7317;//   erass123 complete 
 Emin=0.613; Emax=0.693;
 order=7;
 healpix_event_filename="healpix_for_nicola_E_.613_.693_erass123_order_7_testnew.fits";
 }

 cout<<"nicola="<<nicola<< " event_filename_list_infile="<<event_filename_list_infile<<" nline="<<nline<<endl;


    
 debug=0;

 // copied to here from above 20210721
  read_event_filename_list(event_filename_list_infile,event_filename_list,nline,debug);

  cout<<"event_filename_list.size()= "<<event_filename_list.size()<<endl;
    
  debug=0;

 esass_skymaps_to_healpix(event_filename_list,esass_skymaps_directory,order, coordsys, healpix_event_filename, Emin,Emax, debug);

  cout<<"===   esass_maps_pixlist_to_healpix_cleanup.cc complete"<<endl;

  return 0;
}
