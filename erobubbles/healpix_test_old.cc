

using namespace std;
#include<iostream>
#include<cstdio>               //AWS20190612 since required now apparently, change from 2011-2013 compilers?

#include "healpix_map.h"
#include "healpix_map_fitsio.h"

#include "alm.h"
#include "alm_healpix_tools.h" //v2.20a, v3.11
#include "alm_fitsio.h"
#include "alm_powspec_tools.h"
#include "powspec.h"

#include "xcomplex.h"
#include "arr.h"
#include "fitshandle.h"

///////////////////////////////////////////////////////////////////////////////////

             
int main(int argc, char*argv[]){

  cout<<endl<< "=== healpix_fermi_intensity_skymap v2.0 ==="<<endl<<endl;

 int debug=0;

 if(debug==1) cout<<"argc="<<argc<<endl;

 if(argc!=5) {cout<<" incorrect number of arguments ! usage: "                      <<endl
		  <<"   ./healpix_skymap_convert count_filename exposure_filename FWHM prefix" <<endl;
             exit(0);} 

//cout<<"argv= ";  for(int i=0;i<argc;i++){cout<<argv[i]<<" ";} cout<<endl;
//cout<<"argv[0]="<<argv[0]<<endl;// name of executable

  
  cout<<"input file 1: "  <<argv[1]<<endl;
  cout<<"input file 2: "  <<argv[2]<<endl;
  cout<<"smoothing FWHM= "<<argv[3]<<endl;
  cout<<"prefix =        "<<argv[4]<<endl;
 
 int dividefiles;
 
 if(argc==5) dividefiles=1;

 if(dividefiles==1) cout<<"dividing input file 1 by input file 2"<<endl;  //always true but keep for future enhancements
  
    string infile,outfile;
    string infile1;
    string infile2;        //AWS20120127
    string prefix;
      
                infile =string(argv[1]);

                infile1=string(argv[1]);
  if(argc==5)   infile2=string(argv[2]);

  if(argc==5)   prefix =string(argv[4]);

  // use prefix not suffix to preserve .fits if present in input file
  string USING = "using_";

  string outfile_energies           =              prefix+"_healpix_intensity_"                           +USING +infile1; 
  string outfile_energies_smoothed  =              prefix+"_healpix_intensity_smoothed_"                  +USING +infile1;

  string outfile_integrated_energies_up=              prefix+"_healpix_intensity_integrated_up_"             +USING +infile1;
  string  infile_integrated_energies_up=              prefix+"_healpix_intensity_integrated_up_"             +USING +infile1;
  string outfile_integrated_energies_up_smoothed=     prefix+"_healpix_intensity_integrated_up_smoothed_"    +USING +infile1;

  string outfile_integrated_energies_down=         prefix+"_healpix_intensity_integrated_down_"           +USING +infile1;//AWS20191128
  string  infile_integrated_energies_down=         prefix+"_healpix_intensity_integrated_down_"           +USING +infile1;//AWS20191128
  string outfile_integrated_energies_down_smoothed=prefix+"_healpix_intensity_integrated_down_smoothed_"  +USING +infile1;//AWS20191128



  string outfile_Alm                               =prefix+"_healpix_intensity_Alm_"                      +USING +infile1;
  string outfile_2_rebinned                        =prefix+"_healpix_exposure_rebinned_"                  +USING +infile2; 

  double fwhm_deg=1.0;
        
  if(argc==5)sscanf(argv[3],"%lf",&fwhm_deg ); // l needed for double

  if (debug==1)  cout<<"smoothing FWHM= "<<fwhm_deg<<" deg"<<endl;

  PDT datatype = PLANCK_FLOAT64; // HealPix defines it this way
      datatype = PLANCK_FLOAT32;

  /////////////////////////////////////////////////////////
  // various operations on Fermi HealPix skymaps         //
  /////////////////////////////////////////////////////////

    cout<<" ==== various operations on Fermi HealPix skymaps"<<endl;

  // use healpix io rather than cfitsio even at low level to simplify dependence
  // see Healpix_2.20a/src/cxx/cxxsupport/fitshandle.h
  

 

  
  int npix; 
  int i,j,k;
  long ndata;
  double **val;
  arr<double>data; // for HealPix array class
  int nenergy;
  int hdu;
  fitshandle inputhandle;
  int colnum;

  /* ----------------------

   ///////////////////////////////////////////////////////////////////
    //                            COUNTS                             //
    ///////////////////////////////////////////////////////////////////

  cout<<"reading counts file "<<infile1<<endl;

  inputhandle.open(infile1); 
  inputhandle.goto_hdu(2);
  
  int ncolnum1=inputhandle.ncols();
  cout<<" number of columns ="<<ncolnum1<<endl;
 
  nenergy=ncolnum1; // number of energies = number of columns of counts.  must be a better way

  Healpix_Map<double>inmap1;

 for (int colnum=1;colnum<=ncolnum1;colnum++)

 {
   cout<< "reading input file 1  column healpix: "<<infile1<<",  column number: "<< colnum;

   read_Healpix_map_from_fits(infile1,inmap1,colnum,2);// HDU 2 (1=primary)  AWS20190612 read column exposure

   cout<<" Npix  = "<<inmap1.Npix(); 
   cout<<" Nside = "<<inmap1.Nside();
   cout<<" Order = "<<inmap1.Order();
   cout<<" Scheme= "<<inmap1.Scheme()<<endl; // 0 = RING, 1 = NESTED

 
   npix=inmap1.Npix(); 
 
   if(colnum==1) // allocate the first time since have the sizes
   {
    val=new double*[npix];
    for( i=0;i<npix;i++)val[i]=new double[nenergy];
    cout<<"allocated val"<<endl;
   }

   debug=0;

   j=colnum-1;
   for( i=0;i<npix;i++)  
   {
      if(debug==1) cout<<"counts: energy j="<<j<<"  pixel "<<"i= "<<i<<"  inmap1= "<< inmap1[i]<<endl;
      val[i][j]=inmap1[i];
      if(debug==1) cout<<i<<" "<<j<<" val[i][j]="<< val[i][j]<<endl;
    }



 }// colnum

 //----------------------------------------------------

 
  
  cout<<"end of read counts"<<endl;

   ---------*/


  //////////////////////////////////////////////////////////
  //                     EXPOSURE                         //
  //////////////////////////////////////////////////////////


  //==================== read input file 2 
  
  cout<<endl<<endl<<" reading exposure from "<<infile2<<endl;

  fitshandle in;
  
  in.open(infile2); 
  in.goto_hdu(2);

  
  int ncolnum=in.ncols();
  cout<<" number of columns ="<<ncolnum<<endl;

  Healpix_Map<double> map;

  
 // npix=12 Nside^2 where Nside=2^order


  int order=log(sqrt(npix/12.))/log(2.)+.1;// AWS20190612
  cout<<"HealPix order of input file 1 computed from input file 1 npix = "<<order<<endl;

  order=4; //test! 


  // input map2 to be converted to order of input file 1
  Healpix_Map<double> map_RING_out(order,RING);

  
  outfile= outfile_2_rebinned;
  cout<<"writing to "<<outfile<<endl;

  fitshandle out;  
  out.create("!"+outfile); // ! to overwrite

  
  arr<string> colname;
  colname.alloc(ncolnum);

  for(j=0;j<ncolnum;j++)   colname[j]=in.colname(j+1);

  prepare_Healpix_fitsmap(out,map_RING_out, datatype, colname); // colname is array,  filled above
  

  out.set_key(string("EXTNAME"),string("SKYMAP"));// extension HDU name
   
    

  debug=1;

  for (colnum=1;colnum<=ncolnum;colnum++)

  {
    cout<< "reading input file 2  exposure column healpix: "<<infile2<<",  column number: "<< colnum;

   read_Healpix_map_from_fits(infile2,map,colnum,2);// HDU 2 (1=primary)  AWS20190612 read column exposure

   cout<<" Npix  = "<<map.Npix(); 
   cout<<" Nside = "<<map.Nside();
   cout<<" Order = "<<map.Order();
   cout<<" Scheme= "<<map.Scheme()<<endl; // 0 = RING, 1 = NESTED
  

   //   cout<<"converting exposure map order: "<<map.Order()<<" to order of counts map: "<<inmap1.Order()<<endl;
   cout<<"converting exposure map order: "<<map.Order()<<" to user defined order  "<<map_RING_out.Order()<<endl;

   map_RING_out.Import(map); // converts map order to user specified, result in map_RING_out  

   cout<< "map_RING_out: ";
   cout<<" Npix  = "<<map_RING_out.Npix(); 
   cout<<" Nside = "<<map_RING_out.Nside();
   cout<<" Order = "<<map_RING_out.Order();
   cout<<" Scheme= "<<map_RING_out.Scheme()<<endl; // 0 = RING, 1 = NESTED
  
   out.write_column(colnum,map_RING_out.Map()); // fitshandle.h    output rebinned exposure        

 }// colnum

 out.close();                                                         

 ////////////////////////////////////////////////////////



 
 
  cout<<endl<<" ==== healpix_test complete"<<endl;

 return 0;
  
}
