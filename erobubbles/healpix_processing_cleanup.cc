

using namespace std;
#include<iostream>
#include<cstdio>             

#include "healpix_map.h"
#include "healpix_map_fitsio.h"

#include "alm.h"
#include "alm_fitsio.h"
#include "alm_powspec_tools.h"
#include "alm_healpix_tools.h"
#include "powspec.h"

#include "xcomplex.h"
#include "arr.h"
#include "fitshandle.h"

#include "pointing.h"
 
#include "trafos.h"
// #include "cxxsupport.h"  // trafo.h  need in compile path

///////////////////////////////////////////////////////////////////////////////////

             
int healpix_skymap()
{

  cout<<endl<< "=== healpix_skymap"<<endl<<endl;

  int debug=0;
    
  PDT datatype = PLANCK_FLOAT64; // HealPix defines it this way
      datatype = PLANCK_FLOAT32;
  
 
  arr<double>data; // for HealPix array class
  
  int hdu;
  
  int ncolnum,colnum;
  int order;
  string outfile;


  //==================== 

  outfile   ="healpix_test_out.fits"          ;   

  order=6; 
  ncolnum=1;

  cout<<"test writing healpix skymap to "<<outfile<<endl;


  fitshandle out;  
  out.create("!"+outfile); // ! to overwrite

  arr<string> colname;
  colname.alloc(ncolnum);

  for(int j=0;j<ncolnum;j++)   colname[j]="bubbles";

  Healpix_Map<double> map_RING_out(order,RING);
  prepare_Healpix_fitsmap(out,map_RING_out, datatype, colname);
  
  out.set_key(string("EXTNAME"),string("SKYMAP"));// extension HDU name
     
  colnum=1;
 
  cout<< "map_RING_out: ";
  cout<<" Npix  = "<<map_RING_out.Npix(); 
  cout<<" Nside = "<<map_RING_out.Nside();
  cout<<" Order = "<<map_RING_out.Order();
  cout<<" Scheme= "<<map_RING_out.Scheme()<<endl; // 0 = RING, 1 = NESTED

  data.alloc(map_RING_out.Npix());
  
  pointing pointing_;
  
  double l,b;
  double ra,dec;
  double rtd=180./acos(-1.); // radians to degrees 180/pi

  for (int ipix=0; ipix<map_RING_out.Npix(); ipix++)
  {
   pointing_ = map_RING_out.pix2ang(ipix);

   l =     pointing_.phi  *rtd;
   b = 90.-pointing_.theta*rtd;

   data[ipix]= l * b ;

   if(0)
   cout<<"ipix="<<ipix<<" theta="<<pointing_.theta<<" phi="<<pointing_.phi
       <<" l="<<l<<" b="<<b<<" data[ipix]= "<<data[ipix]<<endl;
  }

  if(0)
  out.write_column(colnum,data); // fitshandle.h


  out.close();                                                         



  /////////// DAtool erass1 healpix
  cout<<endl<<"===============DAtool erass1 healpix"<<endl;

  int Nside_factor            =  4;//8;//16;//64; 
  int replace_raw_by_smoothed =  0; 
  int write_maps              =  1;

  cout<<"Nside_factor="<<Nside_factor<<endl;
  cout<<"replace_raw_by_smoothed="<<replace_raw_by_smoothed<<endl;
  cout<<"write_maps="<<write_maps<<endl;

  /////////////////////////////////////////////
  // read
  string infile;
  string infile_raw,infile_smoothed,infile_rate,infile_exp;
  string                                        infile_exp2;

  int band;
  for(band=21;band<=21;band++)
  
  {
  if(band==21)infile_smoothed="Map_eRASS1_ImageTot_band021_946.fits"; 
  if(band==21)infile_raw     ="Map_eRASS1_ImageTot_band021_rawcounts_946.fits";

  if(band==21)infile_raw     ="healpix_events_equatorial.fits";

  if(band==21)infile_rate    =    "Map_eRASS1_rate_band021_946.fits";

  if(band==21)infile_exp     =    "Map_eRASS1_exp_band021_946.fits";
  if(band==21)infile_exp2    =    "Map_eRASS2_exp_band021_946.fits";
  
  if(band==22)infile_smoothed="Map_eRASS1_ImageTot_band022_946.fits"; 
  if(band==22)infile_raw     ="Map_eRASS1_ImageTot_band022_rawcounts_946.fits";
  if(band==22)infile_rate    =    "Map_eRASS1_rate_band022_946.fits";
  if(band==22)infile_exp     =    "Map_eRASS1_exp_band022_946.fits";
  
  if(band==23)infile_smoothed="Map_eRASS1_ImageTot_band023_946.fits"; 
  if(band==23)infile_raw     ="Map_eRASS1_ImageTot_band023_rawcounts_946.fits";
  if(band==23)infile_rate    =    "Map_eRASS1_rate_band023_946.fits";
  if(band==23)infile_exp     =    "Map_eRASS1_exp_band023_946.fits";

  


  Healpix_Map<double>inmap_smoothed,inmap_raw,inmap_rate,inmap_exp,inmap_exp2,inmap_raw2_RING,inmap_exp_work;
  Healpix_Map<double>inmap_exp_selected;
  Healpix_Map<double>inmap_raw_div_exp;
  Healpix_Map<double>inmap_smoothed_reorder,inmap_raw_reorder,inmap_rate_reorder,inmap_exp_reorder,inmap_raw_div_exp_reorder,inmap_raw_reorder_div_exp_reorder;
  Healpix_Map<double>inmap_raw_div_exp_reorder_RING,inmap_raw_div_exp_reorder_Galactic_NESTED;

  Healpix_Map<double>inmap_smoothed_RING,inmap_raw_RING,inmap_rate_RING,inmap_exp_RING;
  Healpix_Map<double>inmap_raw_div_exp_RING,inmap_raw_div_exp_Galactic_NESTED;
  Healpix_Map<double>                               inmap_raw_Galactic_NESTED;

  colnum=1;
  hdu=2;// HDU 2 (1=primary)  

   cout<<endl;
   
   // smoothed needed still in alm
   cout<< "reading  file column healpix: "<< infile_smoothed<<",  column number: "<< colnum;
   read_Healpix_map_from_fits(infile_smoothed,inmap_smoothed,colnum,hdu);// HDU 2 (1=primary)  

   cout<<" Npix  = "<<inmap_smoothed.Npix(); 
   cout<<" Nside = "<<inmap_smoothed.Nside();
   cout<<" Order = "<<inmap_smoothed.Order();
   cout<<" Scheme= "<<inmap_smoothed.Scheme()<<endl; // 0 = RING, 1 = NESTED
   

   cout<< "reading  file column healpix: "<< infile_raw<<",  column number: "<< colnum;
   read_Healpix_map_from_fits(infile_raw     ,inmap_raw2_RING     ,colnum,hdu);// HDU 2 (1=primary)  

   cout<<" Npix  = "<<inmap_raw2_RING     .Npix(); 
   cout<<" Nside = "<<inmap_raw2_RING     .Nside();
   cout<<" Order = "<<inmap_raw2_RING     .Order();
   cout<<" Scheme= "<<inmap_raw2_RING     .Scheme()<<endl; // 0 = RING, 1 = NESTED

   /* now work in RING always
  cout<<"converting counts from  RING to NESTED for compatibility with exposure"<<endl;

  inmap_raw.Set             (inmap_raw2_RING.Order(),NEST);
  inmap_raw.Import_nograde  (inmap_raw2_RING);
  
  cout<<"after conversion inmap_raw2_RING from _RING to inmap_raw NESTED"<<endl;
   */
   inmap_raw = inmap_raw2_RING; // later replace by read directly
 

   cout<< "reading  file column healpix: "<< infile_exp<<",  column number: "<< colnum;
   read_Healpix_map_from_fits(infile_exp     ,inmap_exp     ,colnum,hdu);// HDU 2 (1=primary)  

   cout<<" Npix  = "<<inmap_exp     .Npix(); 
   cout<<" Nside = "<<inmap_exp     .Nside();
   cout<<" Order = "<<inmap_exp     .Order();
   cout<<" Scheme= "<<inmap_exp     .Scheme()<<endl; // 0 = RING, 1 = NESTED

   cout<<"converting infile_exp from NESTED to RING"<<endl;
   inmap_exp_work.Set             (inmap_exp.Order(),RING);
   inmap_exp_work.Import_nograde  (inmap_exp);
   inmap_exp = inmap_exp_work;


   cout<< "reading  file column healpix: "<< infile_exp2<<",  column number: "<< colnum;
   read_Healpix_map_from_fits(infile_exp2     ,inmap_exp2     ,colnum,hdu);// HDU 2 (1=primary)  

   cout<<" Npix  = "<<inmap_exp2     .Npix(); 
   cout<<" Nside = "<<inmap_exp2     .Nside();
   cout<<" Order = "<<inmap_exp2     .Order();
   cout<<" Scheme= "<<inmap_exp2     .Scheme()<<endl; // 0 = RING, 1 = NESTED

   cout<<"converting infile_exp2 from NESTED to RING"<<endl;
   inmap_exp_work.Set             (inmap_exp2.Order(),RING);
   inmap_exp_work.Import_nograde  (inmap_exp2);
   inmap_exp2 = inmap_exp_work;






   double pi=acos(-1.);
   double dtr=acos(-1.)/180.; // degrees to radians
   double rtd=180./acos(-1.); // radians to degrees
   double solid_angle_pixel=4*pi/inmap_exp.Npix();
   double sa_arcminsq=pow(1/(rtd*60.),2); // arcminsq in sr
   double pixel_arcminsq=solid_angle_pixel/sa_arcminsq;
   double pixel_arcmin_side=sqrt(pixel_arcminsq);
   double solid_angle_datool=pow(4./(rtd*60.*60.) ,2); // 4 arcsecsq in sr

   cout<<endl;
   cout<<"radians to degrees=rtd="<<rtd<<endl;
   cout<<"solid angle of healpix pixel in sr = "<<solid_angle_pixel<<endl;
   cout<<"solid angle 4 arcsecsq used in DAtool in sr = "<<solid_angle_datool<<endl;
   cout<<"healpix/DAtool solid angle= "<< solid_angle_pixel/solid_angle_datool<<endl;
   cout<<"arcmin sq="<<sa_arcminsq<<" sr"<<endl;
   cout<<"healpix pixel_arcminsq="<<pixel_arcminsq<< " arcminsq "<<endl;
   cout<<"healpix pixel side="<<pixel_arcmin_side <<"  arcmin"   <<endl;
   cout<<endl;

  
  ///////////////////////////////////////////////////////

 cout<<endl<<"NB exposure is non-zero also on Russian side!! so do not use <if (exp>0)> for statistics or intensity, rather use smoothed counts or rate which is smoothed. raw counts have valid zeros so also do not use <if (rawcounts>0)> for this"<<endl;
 cout<<"but smoothed counts have also valid zeros so should use reordered smoothed counts for better test"<<endl<<endl;

   double value;
   double sum,sum_smoothed,sum_raw,sum_rate,sum_exp,sum_exp_selected;
   double smoothed_div_exp,smoothed_div_exp_sacorr,raw_div_exp,rate_sacorr;
   double sum_smoothed_div_exp_sacorr,sum_rate_sacorr;

   inmap_exp_selected = inmap_exp;
   inmap_raw_div_exp  = inmap_raw;

  
   int verbose=0;

   sum = sum_smoothed = sum_smoothed_div_exp_sacorr = sum_raw = sum_rate =sum_rate_sacorr =sum_exp=sum_exp_selected=0;

  for (int ipix=0; ipix<inmap_exp.Npix(); ipix++)
  {
   pointing_ = inmap_exp.pix2ang(ipix);

   ra  =     pointing_.phi  *rtd;
   dec = 90.-pointing_.theta*rtd;

 
   if(!(inmap_raw     [ipix]>0))inmap_raw     [ipix]=0.; //-ve->0
        
   if(!(inmap_exp     [ipix]>0))inmap_exp     [ipix]=0.; //nan->0  
   if(!(inmap_exp2    [ipix]>0))inmap_exp2    [ipix]=0.; //nan->0  
 
   // exposure is non-zero also on Russian side so use smoothed counts to select valid pixels
   //if(!(inmap_smoothed[ipix]>0))inmap_exp     [ipix]=0.;  AWS20201124 now keep inmap_exp intact for reorder
 
   //cout<<"infile_exp+=infile_exp2:"<<endl;
   inmap_exp[ipix] += inmap_exp2[ipix];

   // need to fix this test, works anyway
   //   inmap_exp_selected[ipix]=0;
   //   if(inmap_smoothed[ipix]>0)

   inmap_exp_selected[ipix] = inmap_exp[ipix];

  

   value=inmap_raw      [ipix];   
   if(value>0) sum_raw +=value;

  
   value=inmap_exp      [ipix];   
   if(value>0) sum_exp+=value; 

   value=inmap_exp_selected[ipix];
   if(value>0) sum_exp_selected += value;

 
   raw_div_exp=0;
   if(inmap_exp_selected[ipix]>0)   raw_div_exp  = inmap_raw[ipix]/inmap_exp_selected[ipix];    
   inmap_raw_div_exp[ipix] = raw_div_exp;

  

   if(verbose==1||ipix<10||ipix>inmap_raw.Npix()-10)
   cout<<"ipix="<<ipix<<" theta="<<pointing_.theta<<" phi="<<pointing_.phi
       <<" ra="<<ra<<" dec="<<dec
       <<" raw="<<inmap_raw[ipix]
       <<" exp="<<inmap_exp[ipix]
       <<" exp_selected="<<inmap_exp_selected[ipix]
       <<" raw_div_exp="<<     raw_div_exp
       <<endl;
  }
  
  cout<<"band            = "<<band            <<endl;
  cout<<"sum_raw         =" <<sum_raw         <<endl;
  cout<<"sum_exp         =" <<sum_exp         <<endl;
  cout<<"sum_exp_selected=" <<sum_exp_selected<<endl;




  double sum_smoothed_times_solid_angle_pixel_divdatool;
  double      sum_raw_times_solid_angle_pixel_divdatool;
  double      sum_rate_times_solid_angle_pixel_divdatool;

  
  int writetest=0;
  if(writetest==1)
    {
  //// write without fitshandle, to column 1 only
  outfile="!test_healpix_out2.fits"; //! to overwrite

  cout<< "writing  file  healpix: "<<outfile<<",  column number: "<< colnum;

  //https://healpix.sourceforge.io/html/Healpix_cxx/group__healpix__map__fitsio__group.html

   
    }

              

 

   if(0)
   {

    if(band==21)outfile="!test_healpix_raw_div_exp_Equatorial_band021";
    if(band==22)outfile="!test_healpix_raw_div_exp_Equatorial_band022";
    if(band==23)outfile="!test_healpix_raw_div_exp_Equatorial_band023";

                outfile=outfile+"_RING";
                outfile=outfile+".fits";   

   cout<<"writing raw_div_exp  to "<<outfile<<endl;

   cout<<" Npix  = "<<inmap_raw_div_exp     .Npix(); 
   cout<<" Nside = "<<inmap_raw_div_exp     .Nside();
   cout<<" Order = "<<inmap_raw_div_exp     .Order();
   cout<<" Scheme= "<<inmap_raw_div_exp     .Scheme()<<endl; // 0 = RING, 1 = NESTED
   
    write_Healpix_map_to_fits(outfile,inmap_raw_div_exp,datatype);
   }


  int new_Nside = inmap_raw.Nside()/Nside_factor; // power of 2 for Nested  WHAT ABOUT RING?
 


  cout<<"reorder inmap_raw div exp converted to Nside="<<new_Nside<<endl;
  
  
  inmap_raw_div_exp_reorder = inmap_raw_div_exp;

  if(Nside_factor>1)
  {
   inmap_raw_div_exp_reorder.SetNside(new_Nside,inmap_raw_div_exp.Scheme());
   inmap_raw_div_exp_reorder.Import_degrade(    inmap_raw_div_exp);
  }  
  cout<<"inmap_raw_div_exp_reorder Npix="<<    inmap_raw_div_exp_reorder.Npix()<<endl;


    if(band==21)outfile="!test_healpix_raw_div_exp_Equatorial_reorder_band021";
    if(band==22)outfile="!test_healpix_raw_div_exp_Equatorial_reorder_band022";
    if(band==23)outfile="!test_healpix_raw_div_exp_Equatorial_reorder band023";

                outfile=outfile+"_RING";
                outfile=outfile+".fits";                 

   cout<<"writing raw_div_exp_reorder  to "<<outfile<<endl;

   cout<<" Npix  = "<<inmap_raw_div_exp_reorder     .Npix(); 
   cout<<" Nside = "<<inmap_raw_div_exp_reorder     .Nside();
   cout<<" Order = "<<inmap_raw_div_exp_reorder     .Order();
   cout<<" Scheme= "<<inmap_raw_div_exp_reorder     .Scheme()<<endl; // 0 = RING, 1 = NESTED
   
   write_Healpix_map_to_fits(outfile,inmap_raw_div_exp_reorder,datatype);


  cout<<"before alm"<<endl;
  


  ///////////////////////////////////////////////////////
  // spherical harmonics and rotation
  // based on fermi skymap code

  cout<<"compute Alm"<<endl;
  /*
  cout<<"converting from NESTED to RING for Alm"<<endl;

  inmap_raw_RING.Set             (inmap_raw.Order(),RING);
  inmap_raw_RING.Import_nograde  (inmap_raw);
  
  cout<<"after conversion to RING"<<endl;
  */

  inmap_raw_RING = inmap_raw;  // already RING 

   verbose=1;  // beware can cause hangup remotely if too long
  if(verbose)
    

      for(int ipix=0;ipix<100;ipix++)
      {
       pointing_ = inmap_raw_RING.pix2ang(ipix);

       ra  =     pointing_.phi  *rtd;
       dec = 90.-pointing_.theta*rtd;

       cout<<"ipix="<<ipix<<" ra="<<ra<<" dec="<<dec<<" inmap_raw_RING="<<inmap_raw_RING[ipix]
	 //            <<" inmap_raw_div_exp_reorder="<< inmap_raw_div_exp_reorder[ipix]
         //   <<" inmap_raw_reorder_div_exp_reorder="<<inmap_raw_reorder_div_exp_reorder[ipix]
         //   <<" erg cm-2 arcmin-2 s-1"
           <<endl;
	}

  int lmax=3*inmap_raw_RING.Nside();// too large values give wrong results, this is recommended value
  int mmax=3*inmap_raw_RING.Nside();

  lmax=4096;
  mmax=4096;

  // fast check
  if(0)
  {
    lmax=100;//4096;
    mmax=100;//4096;
  }

  cout<<"lmax="<<lmax<<" mmax="<<mmax<<endl;

  Alm<xcomplex<double> > alm(lmax,mmax);

  cout<<"map2alm_iter:"<<endl;
  arr<double>  weight;
  weight.alloc(2*inmap_raw_RING.Nside());// must have at least 2*map.Nside() entries
  weight.fill(1.0);

  bool add_alm=false;
    
  //  map2alm(map,alm,weight,add_alm); // NB consider iter version
  //            (0 equivalent to map2alm)
  int num_iter;
  num_iter=3;// 1 for fast check              
  cout<<"map2alm_iter num_iter="<<num_iter<<endl;
    
  map2alm_iter(inmap_raw_RING,alm,num_iter,weight);  //alm_healpix_tools.h 
  
  cout<<"map2alm_iter for inmap_raw_RING complete"<<endl;

  cout<<"alm2map:"<<endl;

  alm2map(alm,inmap_raw_RING);

  cout<<"after alm2map"<<endl;

     for(int ipix=0;ipix<10;ipix++)
      {
       pointing_ = inmap_raw_RING.pix2ang(ipix);

       ra  =     pointing_.phi  *rtd;
       dec = 90.-pointing_.theta*rtd;

       cout<<"before rotate ipix="<<ipix<<" ra="<<ra<<" dec="<<dec<<" inmap_raw_RING="<<inmap_raw_RING[ipix]
	 //            <<" inmap_raw_div_exp_reorder="<< inmap_raw_div_exp_reorder[ipix]
         //   <<" inmap_raw_reorder_div_exp_reorder="<<inmap_raw_reorder_div_exp_reorder[ipix]
         //   <<" erg cm-2 arcmin-2 s-1"
           <<endl;
	}

     


     /*
Rotates alm through the Euler angles psi, theta and phi. The Euler angle convention is right handed, rotations are active.

    psi is the first rotation about the z-axis (vertical)
    then theta about the ORIGINAL (unrotated) y-axis
    then phi about the ORIGINAL (unrotated) z-axis (vertical)
     */

     /* test using Euler angles

     double phi,theta,psi;
     psi=10;  //deg
     theta=20;
     phi=30;
     cout<<"rotate Alm by psi="<<psi<<" theta="<<theta<<" phi="<<phi<<" deg"<<endl;
     rotate_alm(alm,psi*dtr,theta*dtr,phi*dtr);

     cout<<"alm2map"<<endl;
     alm2map(alm,inmap_raw_RING);

  cout<<"after alm2map rotate"<<endl;

     for(int ipix=0;ipix<100;ipix++)
      {
       pointing_ = inmap_raw_RING.pix2ang(ipix);

       ra  =     pointing_.phi  *rtd;
       dec = 90.-pointing_.theta*rtd;

       cout<<" after rotate ipix="<<ipix<<" ra="<<ra<<" dec="<<dec<<" inmap_raw_RING="<<inmap_raw_RING[ipix]
	 //            <<" inmap_raw_div_exp_reorder="<< inmap_raw_div_exp_reorder[ipix]
         //   <<" inmap_raw_reorder_div_exp_reorder="<<inmap_raw_reorder_div_exp_reorder[ipix]
         //   <<" erg cm-2 arcmin-2 s-1"
           <<endl;
	}
     */

     //  /afs/ipp-garching.mpg.de/home/a/aws/Healpix/Healpix_3.60_module_load_gcc/src/cxx/cxxsupport
     cout<<"use Trafo to get rotation matrix Equatorial to Galactic"<<endl;
     double iepoch=2000.;
     double oepoch=2000.;
     
     Trafo trafo(iepoch,oepoch,Equatorial,Galactic);      // cxxsupport  trafos.cc trafos.h
     rotmatrix rm = trafo.Matrix();
     cout<<"rotate_alm"<<endl;
     rotate_alm(alm,rm);

     cout<<"alm2map"<<endl;
     alm2map(alm,inmap_raw_RING);

     cout<<"after alm2map rotate Celestial to Galactic"<<endl;

     for(int ipix=0;ipix<100;ipix++)
      {
       pointing_ = inmap_raw_RING.pix2ang(ipix);

       l  =     pointing_.phi  *rtd;
       b = 90.-pointing_.theta*rtd;

       cout<<" after rotate Cel to Gal ipix="<<ipix<<" l="<<l<<" b="<<b<<" inmap_raw_RING="<<inmap_raw_RING[ipix]
	 //            <<" inmap_raw_div_exp_reorder="<< inmap_raw_div_exp_reorder[ipix]
         //   <<" inmap_raw_reorder_div_exp_reorder="<<inmap_raw_reorder_div_exp_reorder[ipix]
         //   <<" erg cm-2 arcmin-2 s-1"
           <<endl;
	}
     /*
  cout<<"converting raw Galactic from  RING to NESTED"<<endl;

  inmap_raw_Galactic_NESTED.Set             (inmap_raw_RING.Order(),NEST);
  inmap_raw_Galactic_NESTED.Import_nograde  (inmap_raw_RING);
  
  cout<<"after conversion to NESTED"<<endl;
     */

  //////////////////

  cout<<"converting inmap_raw_div_exp from Celestial to Galactic"<<endl;
  cout<<"compute Alm"<<endl;

  /* already RING
  cout<<"converting from NESTED to RING for Alm"<<endl;

  inmap_raw_div_exp_RING.Set             (inmap_raw_div_exp.Order(),RING);
  inmap_raw_div_exp_RING.Import_nograde  (inmap_raw_div_exp);
  
  cout<<"after conversion from NESTED to RING"<<endl;
  */

  inmap_raw_div_exp_RING = inmap_raw_div_exp;
  

  map2alm_iter(inmap_raw_div_exp_RING,alm,num_iter,weight);  //alm_healpix_tools.h 
  
  cout<<"map2alm_iter for inmap_raw_div_exp_RING complete"<<endl;

  cout<<"alm2map inmap_raw_div_exp_RING:"<<endl;

  alm2map(alm,inmap_raw_div_exp_RING);

  cout<<"after alm2map"<<endl;

     for(int ipix=0;ipix<100;ipix++)
      {
       pointing_ = inmap_raw_div_exp_RING.pix2ang(ipix);

       ra  =     pointing_.phi  *rtd;
       dec = 90.-pointing_.theta*rtd;

       cout<<"before rotate ipix="<<ipix<<" ra="<<ra<<" dec="<<dec<<" inmap_raw_div_exp_RING="<<inmap_raw_div_exp_RING[ipix]
      
           <<endl;
	}


     cout<<"rotate_alm"<<endl;
     rotate_alm(alm,rm);

     cout<<"alm2map"<<endl;
     alm2map(alm,inmap_raw_div_exp_RING);

     cout<<"after alm2map rotate Celestial to Galactic"<<endl;

     for(int ipix=0;ipix<100;ipix++)
      {
       pointing_ = inmap_raw_div_exp_RING.pix2ang(ipix);

       l  =     pointing_.phi  *rtd;
       b = 90.-pointing_.theta*rtd;

       cout<<" after rotate Cel to Gal ipix="<<ipix<<" l="<<l<<" b="<<b<<" inmap_raw_div_exp_RING="<<inmap_raw_div_exp_RING[ipix]
           <<endl;
	}

     /*
  cout<<"converting raw div exp Galactic from  RING to NESTED"<<endl;

  inmap_raw_div_exp_Galactic_NESTED.Set             (inmap_raw_div_exp_RING.Order(),NEST);
  inmap_raw_div_exp_Galactic_NESTED.Import_nograde  (inmap_raw_div_exp_RING);
  
  cout<<"after conversion to raw_div_exp_Galactic_NESTED"<<endl;
     */
 

  //////////////////////////////////////////////////////////////////////////////////////////////////////////
  // reordering = rebinning
  //////////////////////////////////////////////////////////////////////////////////////////////////////////

  //https://healpix.sourceforge.io/html/Healpix_cxx/classHealpix__Map.html


  new_Nside = inmap_raw.Nside()/Nside_factor; // power of 2 for Nested
  cout<<"reorder inmap_raw from Nside= "<<inmap_raw.Nside()<<" to Nside= "<<new_Nside<<endl;
  
  inmap_raw_reorder=inmap_raw;
  if(Nside_factor>1)
  {
   inmap_raw_reorder.SetNside(new_Nside,inmap_raw.Scheme());  //NB Scheme=1=Nested
   inmap_raw_reorder.Import_degrade(    inmap_raw);
  }
  cout<<"inmap_raw_reorder Npix="<<    inmap_raw_reorder.Npix()<<endl;

  cout<<"adjusting counts for solid angle after reorder"<<endl;
  double sa_inmap  =pi/inmap_raw        .Npix();
  double sa_reorder=pi/inmap_raw_reorder.Npix();
  double safactor=sa_reorder/sa_inmap;
  cout<<"solid angle inmap="<<sa_inmap<<" solid angle reorder="<<sa_reorder<<" (sr) solid angle factor="<<safactor<<endl;

  for(int ipix=0;ipix<inmap_raw_reorder.Npix();ipix++)
                      inmap_raw_reorder[ipix]*=safactor;

  inmap_exp_reorder=inmap_exp_selected;                               //AWS20201124
  if(Nside_factor>1)
  {
   inmap_exp_reorder.SetNside(new_Nside,inmap_exp_selected.Scheme()); //AWS20201124
   inmap_exp_reorder.Import_degrade(    inmap_exp_selected);          //AWS20201124
  }  
  cout<<"inmap_exp_reorder Npix="<<    inmap_exp_reorder.Npix()<<endl;


  inmap_smoothed_reorder=inmap_smoothed;                               //AWS20201124
  if(Nside_factor>1)
  {
   inmap_smoothed_reorder.SetNside(new_Nside,inmap_smoothed.Scheme()); //AWS20201124
   inmap_smoothed_reorder.Import_degrade(    inmap_smoothed);          //AWS20201124
  }  
  cout<<"inmap_smoothed_reorder Npix="<<    inmap_smoothed_reorder.Npix()<<endl;



  inmap_raw_reorder_div_exp_reorder = inmap_raw_reorder;

  int nused_exp_reorder=0;

  for(int ipix=0;ipix<inmap_raw_reorder.Npix();ipix++)
    {
       inmap_raw_reorder_div_exp_reorder[ipix]=0;
      if                                                             (inmap_exp_reorder[ipix]>0)
      {
       inmap_raw_reorder_div_exp_reorder[ipix]=inmap_raw_reorder[ipix]/inmap_exp_reorder[ipix];
       nused_exp_reorder++;
      }
    }


  cout<<"reorder inmap_raw div exp converted to Nside="<<new_Nside<<endl;
  cout<<"number of non-zero exposure pixels used=  nused_exp_reorder="<<nused_exp_reorder<<endl;
  
  inmap_raw_div_exp_reorder=inmap_raw_div_exp;

  if(Nside_factor>1)
  {
   inmap_raw_div_exp_reorder.SetNside(new_Nside,inmap_raw_div_exp.Scheme());
   inmap_raw_div_exp_reorder.Import_degrade(    inmap_raw_div_exp);
  }  
  cout<<"inmap_raw_div_exp_reorder Npix="<<    inmap_raw_div_exp_reorder.Npix()<<endl;

  ////////////////////////////////////////////////////////////////////////////////////

  cout<<"converting to intensity cm-2 sr-1 s-1"<<endl;
  double sensitive_area = 1400; // 7 TM cm^2
  cout<<"sensitive area="<<sensitive_area<<endl;

   for(int ipix=0;ipix<inmap_raw_reorder.Npix();ipix++)
    inmap_raw_div_exp_reorder       [ipix] /=  (sensitive_area*sa_inmap);  // solid angle  is still as inmap

   for(int ipix=0;ipix<inmap_raw_reorder.Npix();ipix++)
    inmap_raw_reorder_div_exp_reorder[ipix] /= (sensitive_area*sa_reorder); // solid angle is as reorder




 
   cout<<"solid angle of 1 arcminsq arcmin sq="<<sa_arcminsq<<" sr"<<endl;
    
   pixel_arcminsq=sa_reorder/sa_arcminsq;
   cout<<"healpix pixel_arcminsq="<<pixel_arcminsq<< " arcminsq "<<endl;
   pixel_arcmin_side=sqrt(pixel_arcminsq);
   cout<<"healpix pixel side="<<pixel_arcmin_side <<"  arcmin"   <<endl;


   cout<<"converting to intensity photons cm-2 arcmin-2 s-1"<<endl;
 

   for(int ipix=0;ipix<inmap_raw_reorder.Npix();ipix++)
     inmap_raw_div_exp_reorder[ipix]*= sa_arcminsq;

   for(int ipix=0;ipix<inmap_raw_reorder.Npix();ipix++)
     inmap_raw_reorder_div_exp_reorder[ipix]*= sa_arcminsq;

   cout<<"converting to erg cm-2 arcmin-2 s-1"<<endl;
   double keV_to_erg = 1.602e-9;

   double Ex=1.0;//keV
   double Exmin,Exmax;
   if(band==21) {Exmin=0.2; Exmax=0.6;}
   if(band==22) {Exmin=0.6; Exmax=2.3;}
   if(band==23) {Exmin=2.3; Exmax=5.0;}
  
   Ex=(Exmin+Exmax)/2;
   cout<<"band "<<band<<" Exmin="<<Exmin<<" Exmax="<<Exmax<<" average Ex="<<Ex<<" keV"<<endl;

   //   if(band==21) Ex=(0.3+0.8)/2;  old values, to be removed
   //   if(band==22) Ex=(0.8+2.3)/2;
   //   if(band==23) Ex=(2.3+9.0)/2;

   for(int ipix=0;ipix<inmap_raw_reorder.Npix();ipix++)
     inmap_raw_div_exp_reorder[ipix]*= Ex * keV_to_erg;

  for(int ipix=0;ipix<inmap_raw_reorder.Npix();ipix++)
     inmap_raw_reorder_div_exp_reorder[ipix]*= Ex * keV_to_erg;


  double average_intensity1,average_intensity2;
  int nused1,nused2;

  average_intensity1=average_intensity2=0;
  nused1=nused2=0;

  double sum_raw_reorder     =0;
  double sum_smoothed_reorder=0;

   for(int ipix=0;ipix<inmap_raw_reorder.Npix();ipix++)
     { 
       if                            (inmap_exp_reorder[ipix]>0)
       {
        average_intensity1 += inmap_raw_div_exp_reorder[ipix];
        nused1++;        
       }

       if                                    (inmap_exp_reorder[ipix]>0)
       {
        average_intensity2 += inmap_raw_reorder_div_exp_reorder[ipix];
        nused2++;        
       }
   
       sum_raw_reorder          +=inmap_raw_reorder[ipix];
       sum_smoothed_reorder+=inmap_smoothed_reorder[ipix];
     }

   cout<<"band="<<band<<endl;

   cout<<"sum_raw_reorder              ="<<sum_raw_reorder     <<endl;
   cout<<"sum_smoothed_reorder         ="<<sum_smoothed_reorder<<endl;
   cout<<"sum_smoothed_reorder*safactor="<<sum_smoothed_reorder*safactor<<endl;

   average_intensity1/=nused1;
   cout<<"average intensity method 1 in band "<<band<<"  = "<<average_intensity1<<" erg cm-2 arcminsq s-1 for "<<nused1<< " non-zero exposure pixels"<<endl;

   average_intensity2/=nused2;
   cout<<"average intensity method 2 in band "<<band<<"  = "<<average_intensity2<<" erg cm-2 arcminsq s-1 for "<<nused2<< " non-zero exposure pixels"<<endl;




   verbose=1;  // beware can cause hangup remotely if too long
  if(verbose)
    //    for(int ipix=0;ipix<inmap_raw_reorder.Npix();ipix++)

      for(int ipix=0;ipix<20;ipix++)
      {
       pointing_ = inmap_raw_reorder.pix2ang(ipix);

       ra  =     pointing_.phi  *rtd;
       dec = 90.-pointing_.theta*rtd;

       cout<<"ipix="<<ipix<<" ra="<<ra<<" dec="<<dec<<" inmap_raw_reorder="<<inmap_raw_reorder[ipix]
            <<" inmap_raw_div_exp_reorder="<< inmap_raw_div_exp_reorder[ipix]
            <<" inmap_raw_reorder_div_exp_reorder="<<inmap_raw_reorder_div_exp_reorder[ipix]
            <<" erg cm-2 arcmin-2 s-1"<<endl;
	}




  // convert from Celesial to Galactic

  cout<<"converting inmap_raw_div_exp_reorder from Celestial to Galactic"<<endl;
  cout<<"compute Alm"<<endl;

  /* already RING
  cout<<"converting from NESTED to RING for Alm"<<endl;

  inmap_raw_div_exp_reorder_RING.Set             (inmap_raw_div_exp_reorder.Order(),RING);
  inmap_raw_div_exp_reorder_RING.Import_nograde  (inmap_raw_div_exp_reorder);
  
  cout<<"after conversion from NESTED to RING"<<endl;
  */

  inmap_raw_div_exp_reorder_RING = inmap_raw_div_exp_reorder;

  lmax = 3*inmap_raw_div_exp_reorder_RING.Nside();// too large values give wrong results, this is recommended value
  mmax = 3*inmap_raw_div_exp_reorder_RING.Nside();

  
  // fast check
  if(0)
  {
    lmax=100;//4096;
    mmax=100;//4096;
  }

  cout<<"Alm for reorder "<<"lmax="<<lmax<<" mmax="<<mmax<<endl;
  alm.Set(lmax,mmax);

  map2alm_iter(inmap_raw_div_exp_reorder_RING,alm,num_iter,weight);  //alm_healpix_tools.h 
  


  cout<<"map2alm_iter for inmap_raw_div_exp_reorder_RING complete"<<endl;

  cout<<"alm2map inmap_raw_div_exp_reorder_RING:"<<endl;

  alm2map(alm,   inmap_raw_div_exp_reorder_RING);

  cout<<"after alm2map"<<endl;

     for(int ipix=0;ipix<100;ipix++)
      {
       pointing_ = inmap_raw_div_exp_reorder_RING.pix2ang(ipix);

       ra  =     pointing_.phi  *rtd;
       dec = 90.-pointing_.theta*rtd;

       cout<<"before rotate ipix="<<ipix<<" ra="<<ra<<" dec="<<dec<<" inmap_raw_div_exp_reorder_RING="<<inmap_raw_div_exp_reorder_RING[ipix]
      
           <<endl;
	}

     cout<<"rotate_alm"<<endl;
     rotate_alm(alm,rm);

     cout<<"alm2map"<<endl;
     alm2map(alm,inmap_raw_div_exp_reorder_RING);

     cout<<"after alm2map rotate inmap_raw_div_exp_reorder_RING Celestial to Galactic"<<endl;

     for(int ipix=0;ipix<100;ipix++)
      {
       pointing_ = inmap_raw_div_exp_reorder_RING.pix2ang(ipix);

       l  =     pointing_.phi  *rtd;
       b = 90.-pointing_.theta*rtd;

       cout<<" after rotate Cel to Gal ipix="<<ipix<<" l="<<l<<" b="<<b<<" inmap_raw_div_exp_reorder_RING="<<inmap_raw_div_exp_reorder_RING[ipix]
           <<endl;
	}
     /*
  cout<<"converting raw div exp redorder Galactic from  RING to NESTED"<<endl;

  inmap_raw_div_exp_reorder_Galactic_NESTED.Set             (inmap_raw_div_exp_reorder_RING.Order(),NEST);
  inmap_raw_div_exp_reorder_Galactic_NESTED.Import_nograde  (inmap_raw_div_exp_reorder_RING);
  
  cout<<"after conversion to raw_div_exp_reorder_Galactic_NESTED"<<endl;
     */

     //////////////////////////////////////////////////

  if(write_maps==1)
  { 
    if(band==21)outfile="!healpix_raw_Equatorial_band021";
    if(band==22)outfile="!healpix_raw_Equatorial_band022";
    if(band==23)outfile="!healpix_raw_Equatorial_band023";

    if(Nside_factor==  1)outfile=outfile+"_reorder_Nside_factor_1";
    if(Nside_factor==  2)outfile=outfile+"_reorder_Nside_factor_2";
    if(Nside_factor==  4)outfile=outfile+"_reorder_Nside_factor_4";
    if(Nside_factor==  8)outfile=outfile+"_reorder_Nside_factor_8";
    if(Nside_factor== 16)outfile=outfile+"_reorder_Nside_factor_16";
    if(Nside_factor== 32)outfile=outfile+"_reorder_Nside_factor_32";
    if(Nside_factor== 64)outfile=outfile+"_reorder_Nside_factor_64";
    if(Nside_factor==128)outfile=outfile+"_reorder_Nside_factor_128";
    if(Nside_factor==256)outfile=outfile+"_reorder_Nside_factor_256";
    if(Nside_factor==512)outfile=outfile+"_reorder_Nside_factor_512";
                         outfile=outfile+".fits";

    cout<<"writing raw reordered counts to "<<outfile<<endl;
    write_Healpix_map_to_fits(outfile,inmap_raw_reorder,datatype);

    if(band==21)outfile="!healpix_raw_div_exp_Equatorial_band021";
    if(band==22)outfile="!healpix_raw_div_exp_Equatorial_band022";
    if(band==23)outfile="!healpix_raw_div_exp_Equatorial_band023";

    if(Nside_factor==  1)outfile=outfile+"_reorder_Nside_factor_1";
    if(Nside_factor==  2)outfile=outfile+"_reorder_Nside_factor_2";
    if(Nside_factor==  4)outfile=outfile+"_reorder_Nside_factor_4";
    if(Nside_factor==  8)outfile=outfile+"_reorder_Nside_factor_8";
    if(Nside_factor== 16)outfile=outfile+"_reorder_Nside_factor_16";
    if(Nside_factor== 32)outfile=outfile+"_reorder_Nside_factor_32";
    if(Nside_factor== 64)outfile=outfile+"_reorder_Nside_factor_64";
    if(Nside_factor==128)outfile=outfile+"_reorder_Nside_factor_128";
    if(Nside_factor==256)outfile=outfile+"_reorder_Nside_factor_256";
    if(Nside_factor==512)outfile=outfile+"_reorder_Nside_factor_512";
                         outfile=outfile+".fits";



    cout<<"writing raw div exp Equatorial reordered to "<<outfile<<endl;
    write_Healpix_map_to_fits(outfile,inmap_raw_div_exp_reorder,datatype);



    // Galactic coords reorder


    if(band==21)outfile="!healpix_raw_Galactic_band021";
    if(band==22)outfile="!healpix_raw_Galactic_band022";
    if(band==23)outfile="!healpix_raw_Galactic_band023";

    if(Nside_factor==  1)outfile=outfile+"_reorder_Nside_factor_1";
    if(Nside_factor==  2)outfile=outfile+"_reorder_Nside_factor_2";
    if(Nside_factor==  4)outfile=outfile+"_reorder_Nside_factor_4";
    if(Nside_factor==  8)outfile=outfile+"_reorder_Nside_factor_8";
    if(Nside_factor== 16)outfile=outfile+"_reorder_Nside_factor_16";
    if(Nside_factor== 32)outfile=outfile+"_reorder_Nside_factor_32";
    if(Nside_factor== 64)outfile=outfile+"_reorder_Nside_factor_64";
    if(Nside_factor==128)outfile=outfile+"_reorder_Nside_factor_128";
    if(Nside_factor==256)outfile=outfile+"_reorder_Nside_factor_256";
    if(Nside_factor==512)outfile=outfile+"_reorder_Nside_factor_512";
                         outfile=outfile+"_RING";
                         outfile=outfile+".fits";


			 
    cout<<"writing raw counts reordered Galactic RING to "<<outfile<<endl;
    write_Healpix_map_to_fits(outfile,inmap_raw_reorder,datatype);
			 



    if(band==21)outfile="!healpix_raw_div_exp_Galactic_band021";
    if(band==22)outfile="!healpix_raw_div_exp_Galactic_band022";
    if(band==23)outfile="!healpix_raw_div_exp_Galactic_band023";

    if(Nside_factor==  1)outfile=outfile+"_reorder_Nside_factor_1";
    if(Nside_factor==  2)outfile=outfile+"_reorder_Nside_factor_2";
    if(Nside_factor==  4)outfile=outfile+"_reorder_Nside_factor_4";
    if(Nside_factor==  8)outfile=outfile+"_reorder_Nside_factor_8";
    if(Nside_factor== 16)outfile=outfile+"_reorder_Nside_factor_16";
    if(Nside_factor== 32)outfile=outfile+"_reorder_Nside_factor_32";
    if(Nside_factor== 64)outfile=outfile+"_reorder_Nside_factor_64";
    if(Nside_factor==128)outfile=outfile+"_reorder_Nside_factor_128";
    if(Nside_factor==256)outfile=outfile+"_reorder_Nside_factor_256";
    if(Nside_factor==512)outfile=outfile+"_reorder_Nside_factor_512";
                         outfile=outfile+"_RING";
                         outfile=outfile+".fits";


			 
    cout<<"writing raw div exp reordered Galactic RING to "<<outfile<<endl;
    write_Healpix_map_to_fits(outfile,inmap_raw_div_exp_reorder_RING,datatype);
			 

    // Galactic coords original order

    if(band==21)outfile="!test_healpix_raw_Galactic_band021";
    if(band==22)outfile="!test_healpix_raw_Galactic_band022";
    if(band==23)outfile="!test_healpix_raw_Galactic_band023";

    outfile=outfile+"_NESTED";
    /*
    if(Nside_factor==  1)outfile=outfile+"_reorder_Nside_factor_1";
    if(Nside_factor==  2)outfile=outfile+"_reorder_Nside_factor_2";
    if(Nside_factor==  4)outfile=outfile+"_reorder_Nside_factor_4";
    if(Nside_factor==  8)outfile=outfile+"_reorder_Nside_factor_8";
    if(Nside_factor== 16)outfile=outfile+"_reorder_Nside_factor_16";
    if(Nside_factor== 32)outfile=outfile+"_reorder_Nside_factor_32";
    if(Nside_factor== 64)outfile=outfile+"_reorder_Nside_factor_64";
    if(Nside_factor==128)outfile=outfile+"_reorder_Nside_factor_128";
    if(Nside_factor==256)outfile=outfile+"_reorder_Nside_factor_256";
    if(Nside_factor==512)outfile=outfile+"_reorder_Nside_factor_512";
    */
                         outfile=outfile+".fits";

    if(0)
    {
    cout<<"writing raw Galactic to "<<outfile<<endl;

    //    write_Healpix_map_to_fits(outfile,inmap_raw_RING,datatype);
    
    write_Healpix_map_to_fits(outfile,inmap_raw_Galactic_NESTED,datatype);

   
    if(band==21)outfile="!test_healpix_raw_div_exp_Galactic_band021";
    if(band==22)outfile="!test_healpix_raw_div_exp_Galactic_band022";
    if(band==23)outfile="!test_healpix_raw_div_exp_Galactic_band023";

                outfile=outfile+"_NESTED";
                outfile=outfile+".fits";                 

   cout<<"writing raw_div_exp Galactic to "<<outfile<<endl;

    //    write_Healpix_map_to_fits(outfile,inmap_raw_RING,datatype);
    write_Healpix_map_to_fits(outfile,inmap_raw_div_exp_Galactic_NESTED,datatype);
   } //if 0

  }


  } // for band

 ////////////////////////////////////////////////////////

  cout<<endl<<" ==== healpix_test complete"<<endl;

 return 0;
  
}
////////////////////////////////////////////////////////
int main()
{
  cout<<"healpix_processing_cleanup.cc"<<endl;
  healpix_skymap();
  return 0;
}
