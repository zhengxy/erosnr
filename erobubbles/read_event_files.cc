

using namespace std;
#include<iostream>
#include<cstdio>   
#include<cstring>          
#include<valarray>
#include"fitsio.h"

#include "healpix_map.h"
#include "healpix_map_fitsio.h"
#include "arr.h"
#include "fitshandle.h"
#include "pointing.h"  
#include "trafos.h"

#include <sstream>
#include <string>



/////////////////////////////////////////////////////////
int read_event_filename_list(string event_filename_list_infile, valarray<string> &event_filename_list,int nline,int debug)

{
//based partly on ~/propagate/c/gitlab/galplot/source/Galdef.cc
  cout<<"read_event_file_list"<<endl;
  cout<<"event_filename_list_infile="<<event_filename_list_infile<<endl;
  cout<<"nline="<<nline<<endl;
  cout<<"debug="<<debug<<endl; 
 
  char  filename[200];
  strcpy(filename,event_filename_list_infile.c_str());
  
  FILE *ft;
  ft=fopen(filename,"r");
  if(ft==NULL) {
      cout<<"no input event filename list called "<<filename<<endl; return -1;
  }
 


//int Galdef::read_galdef_parameter(char *filename, char *parstring, char *value)


   char input[37];
   int stat=0;

// ls -1 e*fits >> input_testfile
// this give 36 chars due to \0 or newline  but want 35 hence need to truncate

valarray<string> instring;

instring.resize(nline);

for(int iline=0;iline<nline;iline++)
{

    //https://www.cplusplus.com/reference/cstdio/fgets/
    
 fgets(input,800,ft);
    

 // cout<<"printf:"<<endl;
 // printf("%s",input);       // string is \0 terminated
 
 if(debug==1)cout<<"strlen(input)="<<strlen(input)<<endl;

instring[iline]=input;

 if(debug==1)cout<<"instring= "<<instring[iline]<<endl;

cout<<"iline="<<iline<<" instring="<<instring[iline]<<endl;

 if(debug==1)cout<<"instring length before resize="<<instring[iline].length()<<endl;

 instring[iline].resize(35);
 if(debug==1)cout<<"instring length after resize="<<instring[iline].length()<<endl;

/*
 cout<<"testing with explicit event file"<<endl;
strcpy(input,"eb01_108051_020_EventList_c946.fits");
cout<<"strcmp "<<strcmp(input,"eb01_108051_020_EventList_c946.fits")<<endl;
*/

  } // iline


 if(debug==1)
 {
 cout<<"---------testing open on instring"<<endl;

for(int iline=0;iline<nline;iline++)
  {
 
    if(debug==1)
    {
     cout<<"iline="<<iline<<" "<<instring[iline]<<endl;
     cout<<"length="<<instring[iline].length()<<endl;
     cout<<"instring="<<instring[iline]<<endl;
     cout<< "c_str= " <<instring[iline].c_str()  <<endl;
     }

 FILE *eventft;
       eventft=fopen(instring[iline].c_str(),"r");

  if(eventft!=NULL) cout<<"opened file "   <<instring[iline]<< " c_str= " <<instring[iline].c_str()<<" eventft="<<eventft  <<endl;

  if(eventft==NULL) cout<<"no file called "<<instring[iline]<< " c_str= " <<instring[iline].c_str()  <<endl;
  
  fclose(eventft);

}
 }// debug==1
  
   fclose(ft); 

   event_filename_list=instring;

   cout<<"number of event files read="<<event_filename_list.size()<<endl;

   return stat;
}

////////////////////////////////////////////////////////

void read_event_files(string event_filename,valarray<double>&ra,valarray<double>&dec,valarray<double>&energy,int debug)
{
  cout<<"read_event_files"<<endl;
  cout<<"event_filename= "<<event_filename<<endl;

  int verbose=0;

  fitsfile *fptr=0;
  int status=0; // need to initialize to 0  or error
  int hdunum;
  int hdutype;
  long nrows;
  int ncols;
  int colnum;
  int casesen=1;
  char   colname[100];
  string colnamestring;
  int datatype=TDOUBLE;
  long firstrow,firstelem,nelements;
  double *nulval=NULL;
  int    *anynul=NULL;

#define NMAXROWS 10000000
       double RA[NMAXROWS],DEC[NMAXROWS],ENERGY[NMAXROWS];
//     double RA[10000000],DEC[10000000],ENERGY[10000000];



  //int fits_open_file( fitsfile **fptr, char *filename, int mode, int *status)
  fits_open_file( &fptr, event_filename.c_str(), READONLY, &status);
   

  cout<<"status="<<status<<endl;
  fits_report_error(stdout, status);

  hdunum=2;
  fits_movabs_hdu(fptr, hdunum, &hdutype,  &status);
  cout<<"status="<<status<<endl;
  fits_report_error(stdout, status);
  cout<<"hdutype="<<hdutype<<endl;
  cout<<"BINARY_TBL="<<BINARY_TBL<<endl;

  /*  
int fits_get_num_rows(fitsfile *fptr, long *nrows, int *status)
int fits_get_num_cols(fitsfile *fptr, int  *ncols, int *status)

Get the number of rows or columns in the current FITS table. The number of rows is given by the NAXIS2 keyword and the number of columns is given by the TFIELDS keyword in the header of the table.

_______________________________________________________________
int fits_get_colnum(fitsfile *fptr, int casesen, char *template,
                    int *colnum, int *status)
int fits_get_colname(fitsfile *fptr, int casesen, char *template,
                    char *colname, int *colnum, int *status)
  */

  fits_get_num_rows(fptr, &nrows, &status);
  fits_get_num_cols(fptr, &ncols, &status);
  cout<<"nrows="<<nrows<<" ncols="<<ncols<<endl;
  if(nrows>NMAXROWS){cout<<"too many rows for array size "<<NMAXROWS<< " return!"<<endl; ra.resize(0); return ;}

  colnamestring="RA";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  //                             colnamestring.c_str() did not work as argument (but did for fits_open_read above
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;


  //int fits_read_col(fitsfile *fptr, int datatype, int colnum, long firstrow,
  //     long firstelem, long nelements, void *nulval, void *array, 
  //     int *anynul, int *status)

  firstrow =1;
  firstelem=1;
  nelements=nrows;
  
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, RA, 
		 anynul, &status);

   cout<<"fits_read_col status="<<status<<endl;

   if(verbose==1) for(int i=0;i<nelements;i++)   cout<<RA[i]<<endl;


  colnamestring="DEC";
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;

  
   fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, DEC, 
		 anynul, &status);

   cout<<"fits_read_col status="<<status<<endl;

   if(verbose==1)for(int i=0;i<nelements;i++)   cout<<DEC[i]<<endl;

  colnamestring="PI";  //energy in keV
  strcpy(colname,colnamestring.c_str());
  cout<<colnamestring<<" "<<colnamestring.c_str()<<" "<<colname<<endl;
  fits_get_colnum(fptr, casesen, colname,&colnum, &status);
  cout<<"colname= "<<colname<<" colnum="<<colnum<<" status="<<status<<endl;


  fits_read_col(fptr, datatype, colnum, firstrow,
                 firstelem, nelements, nulval, ENERGY, 
		 anynul, &status);

  cout<<"fits_read_col status="<<status<<endl;

if(debug==1)
  for(int i=0;i<nelements;i++)   cout<<"i="<<i<<" RA="<<RA[i]<<" DEC="<<DEC[i]<<" ENERGY="<<ENERGY[i]<<endl;

  cout<<"copy to valarray"<<endl;



  ra    .resize(nelements);               
  dec   .resize(nelements);
  energy.resize(nelements);

  cout<<"ra.size()="<<ra.size()<<endl;

  for(int i=0;i<nelements;i++)
  {
    ra    [i]=RA    [i];
    dec   [i]=DEC   [i];
    energy[i]=ENERGY[i]*1.0e-3; // eV->keV 
  }



  fits_close_file( fptr,  &status);
   

  cout<<"fits_close_file status="<<status<<endl;
  fits_report_error(stdout, status);
  

  cout<<"end read_event_files"<<endl;
  return;
}

////////////////////////////////////////////////////////

int healpix_skymap( valarray<string> event_filename_list,int order, int coordsys,string outfile, double Emin, double Emax, int debug)
{

  
  cout<<endl<< "=== healpix_skymap"<<endl<<endl;

    
  PDT datatype = PLANCK_FLOAT64; // HealPix defines it this way
      datatype = PLANCK_FLOAT32;
  
 
  arr<double>data; // for HealPix array class
  
  int hdu;
  
  int ncolnum,colnum;
 
 
  valarray<double> ra,dec,energy;
  int ipix;
  double total_events_selected;

  //==================== 

  ncolnum=1;

  cout<<"output healpix events skymap= "<<outfile<<endl;
  cout<<"healpix order="<<order<<endl;
  cout<<"healpix coordsys="<<coordsys;
  if(coordsys==1) cout<< " Equatorial";
  if(coordsys==2) cout<< " Galactic"<<endl;
  if(coordsys >2) {cout<<" invalid coordsys"<<endl; return 1;}
  cout<<"Emin="<<Emin<<" Emax="<<Emax<<" keV"<<endl;
  cout<<"debug="<<debug<<endl;

  fitshandle out;  
  out.create("!"+outfile); // ! to overwrite

  arr<string> colname;
  colname.alloc(ncolnum);

  for(int j=0;j<ncolnum;j++)   colname[j]="events";

  Healpix_Map<double> map_RING_out(order,RING);
  prepare_Healpix_fitsmap(out,map_RING_out, datatype, colname);
  
  out.set_key(string("EXTNAME"),string("SKYMAP"));// extension HDU name
     
  colnum=1;
 
  cout<< "map_RING_out: ";
  cout<<" Npix  = "<<map_RING_out.Npix(); 
  cout<<" Nside = "<<map_RING_out.Nside();
  cout<<" Order = "<<map_RING_out.Order();
  cout<<" Scheme= "<<map_RING_out.Scheme()<<endl; // 0 = RING, 1 = NESTED

  data.alloc(map_RING_out.Npix());
  
  pointing pointing_;
  pointing pointing_galactic;
  
  double l,b;
  double rtd=180./acos(-1.); // radians to degrees 180/pi
  double dtr=acos(-1.)/180.; // degrees to radians pi/180

  double iepoch=2000.;
  double oepoch=2000.;
  Trafo trafo(iepoch,oepoch,Equatorial,Galactic); // cxxsupport  trafos.cc trafos.h
  rotmatrix rm = trafo.Matrix();



  for(ipix=0;ipix<map_RING_out.Npix();ipix++) data[ipix]=0;

  for(int ieventfile=0;ieventfile<event_filename_list.size();ieventfile++)
  {
    cout<<"ieventfile="<<ieventfile<<endl;

    read_event_files(event_filename_list[ieventfile],ra,dec,energy,debug);

  cout<<"ra.size()="<<ra.size()<<endl;
  if(ra.size()==0) {cout<<"ra.size=0 signals too many events in file, return 1"<<endl; return 1;}

  for(int i=0;i<ra.size();i++) 
  {  
 
   if(energy[i] >= Emin && energy[i] <= Emax)

   {
   pointing_.phi   =       ra[i] *dtr;
   pointing_.theta = (90.-dec[i])*dtr; 

   // trafos.h
   /*! Transforms the pointing \a ptg and returns the result. */
   //    pointing operator() (const pointing &ptg) const;

   pointing_galactic=trafo(pointing_);
   l =     pointing_galactic.phi   * rtd;
   b = 90.-pointing_galactic.theta * rtd;

   if(coordsys==1)
   ipix=map_RING_out.ang2pix(pointing_);

  if(coordsys==2)
   ipix=map_RING_out.ang2pix(pointing_galactic);

   data[ipix]+=1;

   if(debug==1)
   cout<<"i="<<i<<" ra="<<ra[i]<<" dec="<<dec[i]<<" energy="<<energy[i]
       <<" pointing_galactic.phi="<<pointing_galactic.phi<<" pointing_galactic.theta="<<pointing_galactic.theta
       <<" l="<<l<<" b="<<b
       <<" ipix="<<ipix<<" data[ipix]="<<data[ipix]<<endl;
   }
 }
}

  total_events_selected=0;
  for(ipix=0;ipix<map_RING_out.Npix();ipix++) total_events_selected+=data[ipix];
  cout<<"total events selected in data="<<total_events_selected<<endl; 

  cout<<"writing healpix skymap to "<< outfile << endl;
  out.write_column(colnum,data); // fitshandle.h

  out.close();                                                         

  map_RING_out.Set(data,RING); // data is zero size after call!

  total_events_selected=0;
  for(ipix=0;ipix<map_RING_out.Npix();ipix++) total_events_selected+=map_RING_out[ipix];
  cout<<"total events selected in  map="<<total_events_selected<<endl; 

  /*
  for (int ipix=0; ipix<map_RING_out.Npix(); ipix++)
  {
   pointing_ = map_RING_out.pix2ang(ipix);

   l =     pointing_.phi  *rtd;
   b = 90.-pointing_.theta*rtd;

   if(debug==1)
   cout<<"ipix="<<ipix<<" theta="<<pointing_.theta<<" phi="<<pointing_.phi
       <<" l="<<l<<" b="<<b<<" map_RING_out[ipix]= "<<map_RING_out[ipix]<<endl;
  }
  */

 ////////////////////////////////////////////////////////

  cout<<endl<<" ==== healpix_skymap complete"<<endl;

 return 0;
  
};


////////////////////////////////////////////////////////
int main()
{ 
  string event_filename;
  valarray<double>ra,dec,energy;

  valarray<string> event_filename_list;
  string healpix_event_filename;
  int coordsys;
  int order;
  double Emin,Emax;
  int debug;
  
  int test_read_event_files=0;

  int nevent_files=4;

  event_filename_list.resize(nevent_files);

  event_filename_list[0]="eb02_096054_020_EventList_c946.fits";
  event_filename_list[1]="eb02_096054_020_EventList_c946_copy1.fits";  
  event_filename_list[2]="eb02_096054_020_EventList_c946_copy2.fits";
  
  int i=0;
  //                     erass ra dec               proc
  //                 owner both   if >90 dec=90-xxx
  //                       |   |  |
  event_filename_list[i]="eb01_108051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_121048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_121048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_096054_020_EventList_c946.fits"; i++;
    
  
  nevent_files=62;
  event_filename_list.resize(nevent_files);
     
  i=0;

  //                     erass ra dec               proc
  event_filename_list[i]="eb01_224084_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_224084_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_233093_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_233093_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_340177_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_340177_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_001180_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_001180_020_EventList_c946.fits"; i++;

  event_filename_list[i]="eb01_121048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_121048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_125048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_125048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_129048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_129048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_133048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_133048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_136048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_136048_020_EventList_c946.fits"; i++;

  event_filename_list[i]="eb01_140048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_140048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_144048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_144048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_148048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_148048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_152048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_152048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_156048_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_156048_020_EventList_c946.fits"; i++;


  event_filename_list[i]="eb01_108051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_108051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_112051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_112051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_116051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_116051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_119051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_119051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_123051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_123051_020_EventList_c946.fits"; i++;


  event_filename_list[i]="em01_127051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_127051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_131051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_131051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_135051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_135051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_138051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_138051_020_EventList_c946.fits"; i++;

  
  event_filename_list[i]="em01_142051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_142051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_146051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_146051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_150051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_150051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_153051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_153051_020_EventList_c946.fits"; i++;

  
  event_filename_list[i]="eb01_157051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_157051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_161051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_161051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_165051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_165051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_169051_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_169051_020_EventList_c946.fits"; i++;
  /*
  event_filename_list[i]="eb01_224084_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_224084_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb01_233093_020_EventList_c946.fits"; i++;
  event_filename_list[i]="eb02_233093_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_340177_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_340177_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em01_001180_020_EventList_c946.fits"; i++;
  event_filename_list[i]="em02_001180_020_EventList_c946.fits"; i++;
  */

 if(test_read_event_files==1)
 for(int ieventfile=0;ieventfile<nevent_files;ieventfile++)
 {
   debug=1;
   read_event_files(event_filename_list[ieventfile],ra,dec,energy,debug);

  cout<<"ra.size()="<<ra.size()<<endl;
  for(int i=0;i<ra.size();i++) 
   cout<<"i="<<i<<" ra="<<ra[i]<<" dec="<<dec[i]<<" energy="<<energy[i]<<endl;
 }

 /////////////
  string event_filename_list_infile;
  

  cout<<"reading event filename list from "<<event_filename_list_infile<<endl;
 
  int nline;
  nline=63;
  nline=189;
  int testfile=5;

  if(testfile==5){event_filename_list_infile="input_testfile5"; nline=4878;} // all  ieventfile= 726 seg 

  if(testfile==6){event_filename_list_infile="input_testfile6"; nline=2261;} //em01  370 seg
  if(testfile==7){event_filename_list_infile="input_testfile7"; nline=2261;} //em02 370 seg
  if(testfile==8){event_filename_list_infile="input_testfile8"; nline=2261;} // em01 -1r: reversed iventfile=  no seg, completed but status 104 for some files 
  if(testfile==9){event_filename_list_infile="input_testfile9"; nline=4878;} // all -1r 3858 start 104  1020 status 0 
  if(testfile==10){event_filename_list_infile="input_testfile10"; nline=1;} // one file which gave status 104 with testfile9 but here ok
  if(testfile==11){event_filename_list_infile="input_testfile11"; nline=2261;} // em02 -1r: reversed 1241 status=104 1020 status 0  withfitsclose: no status 104 but seg at 1310

  debug=0;

  read_event_filename_list(event_filename_list_infile,event_filename_list,nline,debug);

  cout<<"event_filename_list.size()= "<<event_filename_list.size()<<endl;
 /////////////


 order=10;
 coordsys=2;   // 1=equatorial, 2=Galactic
 healpix_event_filename="healpix_events.fits";
 Emin=0.3;
 Emax=2.3;
 debug=0;

 healpix_skymap(event_filename_list,order, coordsys, healpix_event_filename, Emin,Emax, debug);

  return 0;
}
