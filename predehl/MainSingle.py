# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 15:47:49 2020
@author: Peter
"""
import ero_pylib as ero
import numpy as np
import matplotlib.pyplot as plt 
from matplotlib.colors import LogNorm
import glob
#-----------------------------------------------------------------------#
#                                                                       #
#   EXAMPLE SCRIPT: USAGE OF SOME FUNCTIONS OF ero_pylib                #
#                                                                       #
#-----------------------------------------------------------------------#
name='GX339-4'                                                          # source name
path = 'SurveyPointSources/'                                            # path
typ = '.fits'                                                           # eSASS event fits file is requied
ergr = [200,2500]                                                       # parameter: energy range
RD_Pos = [255.70708, -48.78882, 0.5]                                    # parameter: Ra, Dec, radius around [deg]
mod=[1,2,3,4,6]                                                         # parameter: which telescope modules

data = ero.evt_read_fits(path+name+typ)                                 # read fits event file
#-----------------------------------------------------------------------  OPTIONAL
X,Y = ero.radec_to_xy(data, RD_Pos)                                     # convert Ra, Dec to X,Y
data = ero.evt_centroid_TM(data, [X,Y,400])                             # do centroiding
data = ero.rem_table(data)                                              # discard all unnecessary entried
l,b = ero.radec_to_galLB(RD_Pos[0], RD_Pos[1])
#-----------------------------------------------------------------------  SELECT (EXAMPLES, optional)
data = ero.evt_select(data, ERGRANGE=ergr, CIRCLE=RD_Pos)               # select energy range and circle around source 
data = ero.evt_select(data, DETRING=[0,192])                            # select detector area (= ring)
data = ero.evt_select(data, PATTERN=[1,2,3,4])                          # select pattern
#-----------------------------------------------------------------------  FUNCTIONS                           
ero.evt_ima_detXY(data)                                                 # image in detector coordinates
ero.evt_ima_skyXY(data, NAME=name, NBINS=200)                           # image in X,Y sky coordinates
ero.evt_ima_radec(data, NAME=name, NBINS=500)                           # image in Ra, Dec
ero.evt_Spectrum(data, NAME=name)                                       # plots spectrum
ero.evt_Lightcurve(data, NAME='GX339-3', TIMERES=1, PLOT=True)          # plots lightcurve
ero.evt_vignet(data, NAME=name)                                         # plots a vignetting curve
ero.evt_att(data, STEP=200)                                             # plots attitude jitter
ero.evt_psf(data, NAME=name, CENTRE=[X,Y,1000], PLOT=True, COLOR=(0.,0.,1.))    # plots PSF
ero.evt_hew(data, NAME=name)                                            # plots encircled energy
