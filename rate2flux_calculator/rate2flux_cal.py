import numpy as np
from astropy.io import fits
from xspec import *
import sys
import os
import glob

##python3 rate2flux_cal.py 304.7 5 1.0 2.3 apec 6.10380 8.04106E-17 1.86549E-16 15 49

if len(sys.argv)>5:
    rates = float(sys.argv[1])
    TM_n = int(sys.argv[2])
    emin = float(sys.argv[3])
    emax = float(sys.argv[4])
    mod = sys.argv[5]
    par = float(sys.argv[6])
    if mod=='apec':
        par2 = float(sys.argv[7])
        Nh = float(sys.argv[8])
        PAT = int(sys.argv[9])
        Area = float(sys.argv[10])
    else:
        Nh = float(sys.argv[7])##cm^-2
        PAT = int(sys.argv[8])
        Area = float(sys.argv[9])
else:
    rates = float(input("the rates (cts/s):"))
    TM_n = int(input('the number of TM involved:'))
    emin = float(input('the energy range starts from (keV):'))
    emax = float(input('to (keV):'))
    mod = input('The spectral model (apec or po):')
    if mod =='apec':
        par = float(input('The temperature of apec (keV):'))
        par2 = float(input('The abundance of apec:'))
    elif mod =='po':
        par = float(input('The index of po:'))
    Nh = float(input('The number density of galactic absorption (10^22 cm^-2):'))
    PAT = int(input('The PATTERN number (1,3,7,15):'))
    Area = float(input('The sky area involved (deg^2):'))

ero_hdu = fits.open("/home/zhengxy/data1/mosaic_mirror/erass1/DEsky/supplyment/ARF_nocorr.fits")
ero_area = ero_hdu[1].data['SPECRESP']
ene_ero = (ero_hdu[1].data['ENERG_LO']+ero_hdu[1].data['ENERG_HI'])/2.

ero_a = np.interp((emin+emax)/2., ene_ero, ero_area)


###in pyXspec
AllModels.clear()
if mod=='apec':
   # m = Model('tbabs*(apec+po)', setPars={1:1.23854E-05, 2:56.1125, 3:5.72093E-17, 4:0, 5:0.899934, 6:4.15673, 7:4.84667E-02})
    m = Model('tbabs*'+mod, setPars={1:Nh, 2:par, 3:par2, 4:0, 5:10})
elif mod=='po':
    m = Model('tbabs*'+mod, setPars={1:Nh, 2:par, 3:0.2067})
    #m = Model(mod, setPars={1:par, 2:0.243})

RMF = "./supplyment/020_RMF_00001_PAT%i_c020.fits" % PAT
ARF = './supplyment/ARF_nocorr.fits'
if not os.path.exists('./temp'):
    os.system('mkdir ./temp')
if os.path.exists('./temp/delete.fak'):
    os.remove('temp/delete.fak')
fs1 = FakeitSettings(response=RMF, arf=ARF, fileName="temp/delete.fak" , exposure =3000) 
AllModels.calcFlux("%.3f %.3f" %(emin, emax))
ero_flux = AllModels(1).flux[0]
AllData.fakeit(1,fs1)
AllData.clear()
sp=Spectrum("temp/delete.fak")
sp.response=RMF
sp.response.arf=ARF
sp.ignore("**-%.3f %.3f-**" %(emin, emax))
#sp.notice("%.3f" % emin)
ero_rate = sp.rate[0]
AllData.clear()


#######
expolist=[   0.3,        0.5,       0.8,     1.65, 3.65, 6.5]
exprange=[[0.2, 0.4], [0.4, 0.6], [0.6, 1.0], [1.0,2.3], [2.3, 5.0], [5.0, 8.0]]


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

whichexp = find_nearest(expolist, np.mean([emin,emax]))
expmin=exprange[whichexp][0]
expmax=exprange[whichexp][1]

sp=Spectrum("./supplyment/TM4_FWC_c020_ClosedSpec_pat15.fits", respFile=RMF)
sp.ignore("**-%.3f %.3f-**" %(emin, emax))
fwc=sp.rate[0]
AllModels.clear()
AllData.clear()
fwc=fwc/0.825
exp_add='./supplyment/'
expfile = glob.glob('%s/*%.3f_%.3fkeV*_exp*fits' % (exp_add, expmin, expmax))[0]
expfile_Nv = glob.glob('%s/*_unVig*fits' % (exp_add))[0]
exp_d = fits.getdata(expfile)
exp_Nv_d = fits.getdata(expfile_Nv)

ratio = np.nanmedian(exp_Nv_d/exp_d)

#print(ero_rate)
#print(ero_flux)
#print(rates/ero_rate*ero_flux)
print(fwc)
print(ratio)
print('##########################Result##########################')
print('The estimated flux in %.3f-%.3f keV are:' % (emin, emax))
print('Instr bkg un-subtracted flux: %e erg/s/cm^2  (%e erg/s/cm^2/keV)' % (rates/ero_rate*ero_flux/TM_n*7, rates/ero_rate*ero_flux/TM_n*7/(emax-emin)))
print('Instr bkg subtracted flux: %e erg/s/cm^2 (%e erg/s/cm^2/keV)' % ((rates-fwc*ratio*Area*TM_n)/ero_rate*ero_flux/TM_n*7, (rates-fwc*ratio*Area*TM_n)/ero_rate*ero_flux/TM_n*7/(emax-emin)))
print('##################About inst bkg###########################')
print('The estimated particle background (inst_bkg) rates (%i TM summed) is %.3e cts/s per 1 deg^2' % (TM_n, fwc*ratio*TM_n))
if Area:
    print('In the consisdered sky area (%.2f deg^2), the instr background takes (roughly) %.3e cts/s in the total %.3e cts/s (~%.2f %%)' % (Area, fwc*ratio*Area*TM_n, rates, fwc*ratio*Area*TM_n/rates*100) )

print('###########################################################')



