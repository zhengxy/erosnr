This script takes large circular region with radius ext [arcmin] around a center (ra,dec) and computes the numbers of skyfields containing data from this region.  The list of skyfield numbers can then be used as input for DATool.

It does that by:
    - looking for the central box, i.e. the one skyfield containing the center of the region. This region will be the first one in the resulting list. 
    - Looking for all boxes whose edge is touched by the target region.
It returns a list of 6-character strings, i.e. the skyfield numbers. It does not currently check who is the owner of the skyfields.
    
Parameters:
    - ra, dec: Right Ascension and Declination of the center of the region, given in decimal degrees.
    - OR lgal, bgal: Galactic longitude and latitude of the desired center.
    - ext: Radius of the target circular region, given in arcmin.
        
Prerequisites:
    - The file SKYMAPS.fits, findable on DATOOL, giving the corners of all skyfields.     

You can call the script from the command line like:

python3 RegionSelector.py --ra=123.45 --dec=23.45 --ext=450

