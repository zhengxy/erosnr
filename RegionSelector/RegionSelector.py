#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 19:08:43 2020

@author: Martin Mayer
"""

import numpy as np
from astropy.io import fits 
import sys, getopt
from astropy.coordinates import SkyCoord
import astropy.units as u

def ExactDistance(ra1,dec1,ra2,dec2): #Computation of exact great-circle distance for really large regions...
    s = np.sin(dec1/180*np.pi)*np.sin(dec2/180*np.pi)
    c = np.cos(dec1/180*np.pi)*np.cos(dec2/180*np.pi)*np.cos((ra1-ra2)/180*np.pi)
    Theta = 180/np.pi*np.arccos(s+c)
    return Theta 

def GetSkyfieldNRs(ra,dec,ext,skyfieldpath='SKYMAPS.fits',UseExact=True): 
    """
    This script takes a large circular region with radius ext [arcmin] around (ra,dec) 
    and computes the skyfield numbers containing data from this region. 
    The list of skyfield numbers can then be used as input for DATool.
    It does that by
        - looking for the central box, i.e. the one skyfield containing the center of the region. 
        This region will be the first one in the resulting list. AND
        - Looking for all boxes whose edge is touched by the target region.
    It returns a list of 6-character strings, i.e. the skyfield numbers. 
    It does not currently check who is the owner of the skyfields.
    
    Parameters:
        - ra, dec: Right Ascension and Declination of the center of the region, given in decimal degrees.
        - ext: Radius of the target circular region, given in arcmin.
        - skyfieldpath: Path to the skyfield information file.
        - UseExact: Whether to use exact distance or small angle approximation for angular distances 
        
    Prerequisites:
        - The file SKYMAPS.fits, findable on DATOOL, giving the corners of all skyfields.     
    
    You can call the script from the command line like:

        python3 RegionSelector.py --ra=123.45 --dec=23.45 --ext=450
    """
        
    Multiplicity = 10## Dissect every egde in 10 times 3 degrees over region extent, to determine overlap
    N = max(4,int(np.ceil(3*60/ext * Multiplicity)))

    if min(ra,360-ra) <= ext/60/np.cos(dec/180*np.pi): ## DECIDE WHETHER IT WRAPS AROUND 0 to 360!
        ShortCut = False
    else:
        ShortCut = True
        
    ### LOAD SKYFIELD DATA
    SKYFIELDS = fits.open(skyfieldpath)[1].data
    FieldNrs = SKYFIELDS.field('SRVMAP')
    RAMIN = SKYFIELDS.field('RA_MIN')
    RAMAX = SKYFIELDS.field('RA_MAX')
    DECMIN = SKYFIELDS.field('DE_MIN')
    DECMAX = SKYFIELDS.field('DE_MAX')
    FieldNrs = ['%06i' % (j) for j in FieldNrs]
    
    ### MATCHING PROCEDURE 
    Match = []
    for j in range(len(FieldNrs)):
        ### SORT OUT MOST OF THE SKYFIELDS FAR AWAY BEFOREHAND!
        if ShortCut:
            if (ra + ext/60/np.cos(dec/180*np.pi)) < RAMIN[j] or (ra - ext/60/np.cos(dec/180*np.pi)) > RAMAX[j]:
    #        if ra  < RAMIN[j] - ext/60/np.cos(dec/180*np.pi) or ra > RAMAX[j] + ext/60/np.cos(dec/180*np.pi):
                continue
        if dec + ext/60 < DECMIN[j] or dec - ext/60 > DECMAX[j]:
            continue
        
        ### FIND CENTRAL SKYFIELD --> Only skyfield whose edges MIGHT not overlap with region
        bord = [RAMIN[j],RAMAX[j],DECMIN[j],DECMAX[j]]
        if dec >= bord[2] and dec < bord[3]:
            if ra >= bord[0] and ra < bord[1]:
#                print('MADE IT',ra,dec,bord)
                Match.append(FieldNrs[j])
                if ra - ext/np.cos(dec/180*np.pi)/60 < bord[0] or ra + ext/np.cos(dec/180*np.pi)/60 > bord[1] or dec - ext/60 < bord[2] or dec + ext/60 > bord[3]:                            
                    continue
                else:
                    break
                
        ### LOOK FOR SKYFIELDS WHICH INTERSECT THE REGION
        # Start by finding edges of regions and dissect into sufficiently finely spaced points
        Corners = np.array([[bord[0],bord[2]], [bord[1],bord[2]], [bord[1],bord[3]], [bord[0],bord[3]]]) ### CORNERS OF THE SKYFIELD BOX!
        Edges = []  
        
        for i in range(4):
            x0,y0 = Corners[i]
            x1,y1 = Corners[(i+1) % 4]
            for n in range(N+1):
                p = n/N
                x2,y2 = np.array([x0,y0])*(1-p) + np.array([x1,y1])*p 
                Edges.append([x2,y2])
#                plt.scatter(x2,y2)
#        plt.show()
                
                
        # Check if points are located within radius of the region.
        Rs = []
        for i,n in enumerate(Edges):
            x,y = n 
            if UseExact:
                r = ExactDistance(ra,dec,x,y)*60
            else:
                r = ((x-ra)**2*np.cos(dec/180*np.pi)**2 + (y-dec)**2)**0.5*60
                
            Rs.append(r)

        if np.min(Rs) < ext:
            Match.append(FieldNrs[j])
            
            
    ### Reduce the final list of matches and remove potential doubles
    Match = list(set(Match))
    
    return Match 


def main(argv):
    try:
        opts, args = getopt.getopt(argv,"r:d:x:u:l:b:",["ra=","dec=","ext=","useexact","lgal=","bgal="])
    except getopt.GetoptError:
        sys.exit(2)
    ra,dec,ext,UseExact,l,b = '','','',True,'',''
    for opt, arg in opts:
        if opt in ("-r","--ra"):
            ra = float(arg)
        if opt in ("-d","--dec"):
            dec = float(arg)
        if opt in ("-l","--lgal"):
            l = float(arg)
        if opt in ("-b","--bgal"):
            b = float(arg)
        if opt in ("-x","--ext"):
            ext = float(arg)
#        if opt == "--useexact":
#            UseExact = True
    
    if l != '' and b != '':### Allow to specify galactic coordinates!
        Coord = SkyCoord(l=l,b=b,unit = (u.deg, u.deg), frame='galactic')
        ra, dec = Coord.fk5.ra.deg, Coord.fk5.dec.deg
        
    if '' in (ra,dec,ext):
        print('Parameter Error! I have read the values',ra,dec,ext,'for ra,dec,ext!')
        sys.exit(2)
        return
    return ra,dec,ext,UseExact
    
""" CALL FROM COMMAND LINE """
if __name__ == "__main__":
   ra,dec,ext,UseExact = main(sys.argv[1:])   
fields = GetSkyfieldNRs(ra = ra, ## Decimal RA
                        dec = dec, ## Decimal Dec 
                        ext = ext, ## Radius of circular region, given in arcmin, so this is 10 degrees
                        skyfieldpath='SKYMAPS.fits', ## Path to fits file giving the skyfield info
                        UseExact=UseExact ## Use exact computation for angular distances, not small angle approximation
                        )
for i in fields:# print out as list of strings
    print(i)
   
""" CALL BY EDITING PARAMETERS DIRECTLY """
#A = GetSkyfieldNRs(ra = 180, ## Decimal RA
#               dec = 0, ## Decimal Dec 
#               ext = 600, ## Radius of circular region, given in arcmin, so this is 10 degrees
#               skyfieldpath='SKYMAPS.fits', ## Path to fits file giving the skyfield info
#               UseExact=True ## Use exact computation for angular distances, not small angle approximation
#               )
